import QtQuick 2.0

import QtQuick.Layouts 1.3

Item
{
    id:side_menu
    width: 150
    anchors.margins: 0
    Layout.fillHeight: true
    Layout.bottomMargin: 4
    Layout.leftMargin: 2


    property string bg_color:"#343434"
    property string button_color:"#888888"
    property int current_button:0

    function setBackgroundColor(color)
    {
        inner_rect.color = color
        outer_rect.color = color

        return color
    }

    function setCurrentButton(button)
    {
        button_1.toggle(false)
        button_2.toggle(false)
        button_3.toggle(false)
        button_4.toggle(false)

        button.toggle(true)

    }

    function clickButton(index){
     //contents_stack.currentIndex = index
     current_button = index

        switch(index){
        case 0:
            button_1.toggle(true)
            button_2.toggle(false)
            button_3.toggle(false)
            button_4.toggle(false)

            window.loadInfoTemplate()

            break;

        case 1:
            button_1.toggle(false)
            button_2.toggle(true)
            button_3.toggle(false)
            button_4.toggle(false)

            break;

        case 2:
            button_1.toggle(false)
            button_2.toggle(false)
            button_3.toggle(true)
            button_4.toggle(false)

            break;

        case 3:
            button_1.toggle(false)
            button_2.toggle(false)
            button_3.toggle(false)
            button_4.toggle(true)

            break;

        }
    }

Rectangle
{

    anchors.fill: parent


 color:bg_color
 id:outer_rect
 border.width: 0
 radius: 0

 Rectangle{
     id:inner_rect
     radius:0
     color:bg_color
     /*gradient: Gradient {
            GradientStop { position: 0.0; color: "#AAAAAA" }
            GradientStop { position: 0.33; color: "#343434" }
            GradientStop { position: 1.0; color: "#666666" }
        }*/

     anchors.fill: parent
     anchors.margins: 0


ColumnLayout {

    id: topmenu
    spacing:0
     Layout.fillWidth: true

     Rectangle{
         Layout.fillWidth: true
         Layout.minimumHeight: 40
         Layout.maximumHeight: 40
         color:"transparent"
     }

     SideMenuButton{
         id:button_1
         Layout.minimumWidth: side_menu.width
         parent_toolbar: side_menu
         text:"Info"
         bg_color: side_menu.bg_color

         onClicked: {
             current_button = 0

             window.loadInfoTemplate()

          }

     }

     SideMenuButton{
         id:button_2
         parent_toolbar: side_menu

         Layout.minimumWidth: side_menu.width
         bg_color: side_menu.bg_color

         text:"Performance";

         onClicked: {

             window.loadPerformanceTemplate()

             current_button = 1
          }
     }
     SideMenuButton{
         id:button_3
         parent_toolbar: side_menu

         Layout.minimumWidth: side_menu.width
         bg_color: side_menu.bg_color

         text:"Risk";

         onClicked: {
             current_button = 2


             window.loadRiskTemplate()
          }
     }
     SideMenuButton{
         id:button_4
         parent_toolbar: side_menu

         Layout.minimumWidth: side_menu.width
         bg_color: side_menu.bg_color

         text:"Returns";

         onClicked: {
             current_button = 3


             window.loadReturnsTemplate()
          }
     }


/*
     RowLayout
     {
         implicitHeight: 200
         implicitWidth: topmenu.width
     Rectangle{
         id:b1
         color: bg_color
         implicitHeight: 200
         implicitWidth: 30
     }
                     Rectangle{
                         id:submenu
                         color: bg_color
                         implicitHeight: 200
                         implicitWidth: topmenu.width-35
                         anchors.left: b1.right

                         ColumnLayout
                         {
                             implicitHeight: 200
                             implicitWidth: submenu.width
                         SideMenuButton{
                             Layout.minimumWidth: submenu.width
                             parent_toolbar: submenu
                             text:"Open"
                             bg_color: submenu.color

                         }

                         SideMenuButton{
                             parent_toolbar: submenu

                             Layout.minimumWidth: submenu.width
                             bg_color: submenu.color

                             text:"Browse";
                         }

                         SideMenuButton{
                             Layout.minimumWidth: submenu.width
                             parent_toolbar: submenu
                             bg_color: submenu.color

                             text:"Info";



                         }}
                     }
     }
*/

    }


 }
 }
}
