#ifndef QML_HTML_TEMPLATE_H
#define QML_HTML_TEMPLATE_H

#include <QObject>
#include <QString>

#include "html_template.h"
#include "sys_metrics.h"

namespace tsarv {
namespace qml {

    class html_template : public QObject, public tsarv::html_template
    {
        Q_OBJECT
    public:
        explicit html_template(QObject *parent = 0);

        Q_INVOKABLE void load_metrics(QString strategy_path);
        Q_INVOKABLE void load_metrics(tsa::metrics* metrics);


        Q_INVOKABLE QString get_template_string();
        Q_INVOKABLE bool set_template(QString template_file);

        Q_INVOKABLE QString get_strategy_name();



    signals:

    public slots:
    protected:
         tsa::metrics* metrics_obj;
    };
}
}
#endif // QML_HTML_TEMPLATE_H
