import QtQuick 2.0
import QtQuick.Controls 2.1

Button {
      id: b1

      property bool button_toggled: false
      property string object_type :"rv_toggle_button"
      property var parent_toolbar : null
      property string color_normal:"#202024"
      property string color_pressed:"#757575"
      property string color_hover_normal:"#505050"
      property string color_hover_pressed:"#888888"

      property string color_text_normal:"white"
      property string color_text_pressed:"white"


      property string bg_color:"#000511"
      property string bg_color_pressed:"#000511"


      function setBackgroundColor(color)
      {
          color_normal = color
          return color
      }



      function toggle(turn_on)
      {
          button_toggled = turn_on
          back.color = button_toggled?color_pressed:color_normal
          button_text.color=button_toggled?color_text_pressed:color_text_normal
         // clicked_sign.visible = button_toggled?true:false

      }

    font.family: "Areal"; font.pointSize: 10; font.bold: true;

        width: 300
          text:"File"


          contentItem:




              Text {
                    id:button_text
                    text: b1.text
                    font: b1.font
                    opacity: enabled ? 1.0 : 0.3
                    //color: b1.down ? "white" : "black"
                    color:color_text_normal
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }


                background: Rectangle {

                    /*Rectangle{
                        id:clicked_sign
                        implicitWidth: 160
                        implicitHeight: 3
                        color: "white"
                        visible: false

                    }*/

                    id:back
                    implicitWidth: 100
                    implicitHeight: 45
                    opacity: enabled ? 1 : 0.3
                    border.color: button_toggled ? "#676767" :color_normal
                    border.width: 0

                    //radius: 4

                    color:color_normal
                }

          MouseArea {
              hoverEnabled: true
              anchors.fill: parent;
              onPressed: {



                  //back.color = button_toggled?"white":"gray"



              }
              onClicked: {

                  if (button_toggled)
                  {
                      button_toggled = false;
                  }
                  else
                  {
                      button_toggled = true;
                  }

                  if( parent_toolbar != null)
                  {
                      parent_toolbar.setCurrentButton(b1)
                  }
                  back.color = button_toggled?color_pressed:color_normal

                  b1.clicked();
              //back.color = button_toggled?"white":"gray"
              }
              onReleased: {
                 // button_text.color='red'
              }
              onEntered:{
                  back.color = button_toggled?color_pressed:color_hover_normal
                  //back.border.color =  button_toggled ? "#F88379" :"#FE6F5E"

              }
              onExited: {back.color = button_toggled?color_pressed:color_normal
                  //back.border.color =  button_toggled ? "#F88379" :"transparent"

              }
              }
          }
