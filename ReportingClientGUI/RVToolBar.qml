import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4 as Controls14
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

ToolBar {

    id:rv_tool_bar
    spacing: 0
    Layout.fillWidth: true
    Layout.minimumHeight: 50
    Layout.maximumHeight: 50
    property string bg_color:"#202024"
    property var settings_drawer:null
    property var contents_stack:null
    property int current_button:0

    //property RVToggleButton current_button:main_menu_button_file

   /* function init()
    {
        //id_rv_button_group.add_button(main_menu_button_file)
        //id_rv_button_group.add_button(main_menu_button_charts)
        //id_rv_button_group.add_button(main_menu_button_reports)
    }*/

    function enableChartOption(){
        main_menu_button_charts.enabled=true
    }

    function setCurrentButton(button)
    {
        main_menu_button_file.toggle(false)
        main_menu_button_charts.toggle(false)
        main_menu_button_reports.toggle(false)

        button.toggle(true)

    }

    function set_strategy_name(strategy_name){
        rv_navigation_bar.setStrategy(strategy_name)
    }

    function get_selected_instrument(){
        return rv_navigation_bar.get_selected_instrument()
    }

    function clear_instrument_combo(){
        rv_navigation_bar.clear_combo()
    }

    function select_first_instrument(){
        rv_navigation_bar.select_first_instrument()
    }

    function add_instrument_to_nav(instrument_name){
        rv_navigation_bar.add_instrument(instrument_name)
    }

    function clickButton(index){
     //contents_stack.currentIndex = index
     current_button = index

        switch(index){
        case 0:
            main_menu_button_file.toggle(true)
            main_menu_button_charts.toggle(false)
            main_menu_button_reports.toggle(false)
            rv_navigation_bar.visible=false
            control_bar.visible=false
            break;

        case 1:
            main_menu_button_charts.toggle(false)
            main_menu_button_file.toggle(false)
            main_menu_button_reports.toggle(true)
            rv_navigation_bar.visible=true
            control_bar.visible=true


            break;

        case 2:
            main_menu_button_reports.toggle(false)
            main_menu_button_file.toggle(false)
            main_menu_button_charts.toggle(true)
            rv_navigation_bar.visible=false
            control_bar.visible=false

            break;

        }
    }

    function setBackgroundColor(color)
    {
        bg.color = color
        return color
    }




            Rectangle{
                id:bg
                anchors.fill: parent
                color:bg_color

RowLayout{
                anchors.fill: parent
                spacing: 0

                RowLayout {
                    id: topmenu
                    Layout.fillWidth: true
                    spacing: 0




                        Image {
                            source: "tsapi_logo.png"
                            //color:"#D52027"
                            height: 60
                            Layout.maximumWidth: 150
                            Layout.minimumHeight: 50
                            Layout.maximumHeight: 50
                        }


                       /* Rectangle{
                            implicitWidth: 50
                            Layout.fillHeight: true
                            color: "transparent"
                        }*/



                     RVToggleButton{
                         id:main_menu_button_file
                         bg_color: rv_tool_bar.bg_color
                         //button_group:id_rv_button_group
                         Layout.minimumWidth: 160
                         Layout.minimumHeight: 50
                         Layout.maximumHeight: 50
                         text:"File";
                         parent_toolbar: rv_tool_bar
                         onClicked: {
                             //contents_stack.currentIndex = 0
                             current_button = 0
                             //menu_stack.visible=true
                             rv_navigation_bar.visible=false
                             control_bar.visible=false

                          }

                     }

                     RVToggleButton{
                         id:main_menu_button_reports
                         //button_group:id_rv_button_group
                         bg_color: rv_tool_bar.bg_color
                         Layout.minimumHeight: 50
                         Layout.maximumHeight: 50
                         parent_toolbar: rv_tool_bar

                         Layout.minimumWidth: 160

                         text:"Reports";

                         onClicked: {
                             //contents_stack.currentIndex = 1
                             current_button = 1
                             //menu_stack.visible=true
                             rv_navigation_bar.visible=true

                             control_bar.visible=true


                          }
                     }

/*
                     Controls14.ComboBox {

                         width: 400

                         style: ComboBoxStyle{
                             textColor: "white"
                             selectedTextColor: "red"
                             selectionColor: "green"

                         }

                         textRole: "text"
                         currentIndex: 2
                         model: ListModel {
                             id: cbItems
                             ListElement { text: "Banana"; color: "Yellow"; }
                             ListElement { text: "Apple"; color: "Green" }
                             ListElement { text: "Coconut"; color: "Brown" }
                         }

                         onCurrentIndexChanged: console.debug(cbItems.get(currentIndex).text + ", " + cbItems.get(currentIndex).color)
                     }*/

                    RVToggleButton {
                        id:main_menu_button_charts
                        enabled: false
                        Layout.minimumHeight: 50
                        Layout.maximumHeight: 50
                        //button_group:id_rv_button_group
                        bg_color: rv_tool_bar.bg_color

                        parent_toolbar: rv_tool_bar

                        Layout.minimumWidth: 160
                        text: "Charts"

                        onClicked: {
                            current_button = 2
                            //menu_stack.visible=false
                            window.loadChart(0)
                            rv_navigation_bar.visible=false
                            control_bar.visible=false


                         }
                     }

                    RVNavigationBar{

                        id:rv_navigation_bar
                        Layout.leftMargin: 100
                        visible: false
                    }


                }

                /*Image {

                    id:settings_icon

                    source: "settings.png"
                    Layout.minimumWidth: 32
                    Layout.minimumHeight: 32
                    Layout.maximumWidth: 32
                    Layout.maximumHeight: 32

                    Rectangle{
                        anchors.fill: parent
                        border.color: "#F88379"
                        border.width: 0
                        radius: 15
                        color: "transparent"
                        MouseArea{
                            anchors.fill: parent
                            onClicked: {

                                if( settings_drawer.is_opened)
                                {
                                    settings_drawer.close()

                                }
                                else
                                {
                                    settings_drawer.open()

                                }

                            }

                            onPressed: {
                                settings_icon.opacity=0.5
                            }
                            onReleased: {
                                settings_icon.opacity=1
                            }
                        }
                    }


                }*/


}

            }


}
