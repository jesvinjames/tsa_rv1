#ifndef CHART_MANAGER_H
#define CHART_MANAGER_H

#include <QObject>

#include <string>
#include "tsc.h"
#include "db_dbase.h"
#include "json_chart.h"


namespace tsarv {
class chart_manager : public QObject
{
    Q_OBJECT
public:
    explicit chart_manager(QObject *parent = 0);
    Q_INVOKABLE void set_strategy_path(QString strategy_path);
    Q_INVOKABLE void set_instrument_number(int instr_no);
    Q_INVOKABLE QString get_price_chart_js_string(int action);
    Q_INVOKABLE QString get_price_chart_html_string();
    Q_INVOKABLE QString get_dummy_price_chart();



    void set_metrices(tsa::metrics* strategy_metrics,tsa::metrics* instrument_metrics );
    ~chart_manager();
protected:
    tsa::fast::database* m_database;
    tsc::report_chart_manager m_chart_manager;
    std::string m_strategy_path;
    int m_instr_no;
    std::string m_instr_table_name;
    std::string price_chart_html_string;

    int m_range_low;
    int m_range_high;
    int m_num_of_bars;
    int m_zoom_factor;
    int m_interval;

    tsa::metrics* m_strategy_metrics;
    tsa::metrics* m_instrument_metrics;






};
}

#endif // CHART_MANAGER_H
