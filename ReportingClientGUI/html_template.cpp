#include "html_template.h"

namespace tsarv
{

    html_template::html_template():regex_expression("\\$\\{[\\w\\|]*\\}")
    {
        template_body = "<html><body>Template not Loaded</body></html>";
    initial_template_body = "<html><body>Template not Loaded</body></html>";
        num_of_keys = 0;
        keys_loaded = false;
        m_loaded = false;
    }

    html_template::html_template(std::string template_file_name):regex_expression("\\$\\{[\\w\\|]*\\}")
    {
        num_of_keys = 0;
        keys_loaded = false;
        template_body = "<html><body>Template not Loaded</body></html>";
        m_loaded = false;
        try
            {


                std::ifstream fileTemplate(template_file_name);

                if (fileTemplate.is_open())
                {

                    std::stringstream bufferFileContents;
                    bufferFileContents << fileTemplate.rdbuf();

                    std::string strFileContents = bufferFileContents.str();
                    template_body = strFileContents;
                    //std::cout<<"\n\n\n\nInitial\n\n\n\n"<<template_body<<"\n\n\n\n"<<std::flush;
                    initial_template_body = strFileContents;

                    load_keys();
                }

            }
            catch (...)
            {
                std::cout << "\nError reading template file\n";
                template_body = "<html><body><div><p><center>Error parsing template file</center></p></div></body></html>";
                initial_template_body = "<html><body><div><p><center>Error parsing template file</center></p></div></body></html>";

        }
    }

    html_template::~html_template()
    {

    }

    bool html_template::set_template_string(std::string template_body)
    {
        this->template_body = template_body;
        return true;
    }

    bool html_template::set_template(std::string strTemplateFileName)
    {
        try
            {


                std::ifstream fileTemplate(strTemplateFileName);

                if (!fileTemplate.is_open()) return false;

                std::stringstream bufferFileContents;
                bufferFileContents << fileTemplate.rdbuf();

                std::string strFileContents = bufferFileContents.str();
                template_body = strFileContents;


               /* std::size_t nBodyTagPOsition = strFileContents.find("<body>");

                if (nBodyTagPOsition != std::string::npos)
                {
                    // If <body> tag exists then split the string
                    std::string strTemplateBody = strFileContents.substr(nBodyTagPOsition);

                    try
                    {
                        strTemplateBody = strTemplateBody.replace(0, 6, "<div>");

                        std::size_t nBodyEndTagPOsition = strTemplateBody.find("</body>");
                        if (nBodyEndTagPOsition != std::string::npos)
                        {
                            strTemplateBody = strTemplateBody.replace(nBodyEndTagPOsition, 7, "</div>");
                        }

                        std::size_t nHtmlEndTagPOsition = strTemplateBody.find("</html>");
                        if (nHtmlEndTagPOsition != std::string::npos)
                        {
                            strTemplateBody = strTemplateBody.replace(nHtmlEndTagPOsition, 7, "       ");
                        }

                        template_body = strTemplateBody;
                        return true;
                    }
                    catch (std::exception e)
                    {
                        std::cout << e.what();
                        template_body = "<html><body><div><p><center>Error parsing template file</center></p></div></body></html>";
                        return false;
                    }
                    //std::cout << "\n" << strTemplateBody << "\n";

                }*/
            }
            catch (...)
            {
                std::cout << "\nError reading template file\n";
                template_body = "<html><body><div><p><center>Error parsing template file</center></p></div></body></html>";
                return false;
            }
    }

    bool html_template::load_keys()
    {
        if(!keys_loaded)
        {
            std::smatch result;



            auto itr = this->template_body.cbegin();
            int i=1;

            while (std::regex_search(itr, this->template_body.cend(), result, this->regex_expression)) {

                //std::cout << i++<<":[" << result[0] << "]" << std::endl;
                this->keys.push_back(result[0]);
                this->values.push_back("notset");
                m_strsKeys[result[0]] = "notset";
                this->num_of_keys++;
                int p =  result.position();
                int l = result.length();
                template_key key;
                key.position = p;
                key.length = l;
                this->key_chars.push_back(key);
                itr += p + l;
            }

            this->keys_loaded = true;
        }

        return true;
    }

    void html_template::set_key(std::string key, std::string data)
    {
        //if (m_strsKeys.count(key) > 0)
        {
            m_strsKeys[key] = data;
            //replace_all(template_body,key, data);

        }

    }

    std::string html_template::process()
    {

        try
        {
        std::smatch result;
        auto itr = this->template_body.cbegin();

        int i=0;


       for( std::map<std::string,std::string>::iterator key_iterator=m_strsKeys.begin();
            key_iterator!=m_strsKeys.end();++key_iterator)
       {

           if( key_iterator->first=="histogram_chart_script|script")
           {
            std::cout<<"\n*@&%#&^%@#@#&&#%@&#@&#%\n\n\n"<<key_iterator->second<<"\n863462^#*^$#^$^#\n";
           }
           //size_t pos = template_body.find(key_iterator->first);
           //template_body.replace( pos, key_iterator->first.size(), key_iterator->second);
           replace_all(template_body,key_iterator->first, key_iterator->second);
           //template_body = std::regex_replace(template_body, std::regex(key_iterator->first), key_iterator->second);


                //std::cout<<std::endl<<"replaced"<<std::endl;

          //  std::cout<<i++;

           // std::cout<<std::endl<<template_body<<std::flush;
            // try string replace from back of the string

        }
        }
        catch(std::exception e)
        {
            std::cout<<"\nError: "<<e.what();
        }

        m_loaded = true;

        return template_body;

    }

    bool html_template::is_loaded()
    {
        return m_loaded;
    }

    void html_template::unload()
    {
        m_loaded = false;
    }

    std::string html_template::template_string()
    {
        return template_body;
    }

    void html_template::set_values(std::vector<std::string> values)
    {
        this->values = values;
    }

    void html_template::set_keys(std::vector<std::string> keys)
    {
        this->keys = keys;
    }

    void html_template::reset_keys()
    {
        for(int i=0;i<num_of_keys;i++)
        {
            values[i] = "notset";
        }
    }

    void html_template::replace_all( std::string &s, const std::string &search, const std::string &replace ) {
        for( size_t pos = 0; ; pos += replace.length() ) {
            // Locate the substring to replace
            pos = s.find( search, pos );
            if( pos == std::string::npos ) break;
            // Replace by erasing and inserting
            s.erase( pos, search.length() );
            s.insert( pos, replace );
        }
    }

    void html_template::clear()
    {
        template_body = initial_template_body;
    }

    void html_template::processTemplate(tsa::metrics* cptrMetrics_i, tsa::metrics* cptrStrategyMetrics_i)
    {
        if (cptrMetrics_i == NULL) return;
int c=0;

        std::string varName;
        try
        {
            for (auto const& key : this->m_strsKeys)
            {

                varName = key.first;
                varName.erase(0, 2);
                varName.pop_back();

             /*   if( key.first == "${trade_count|all|int64}")
                    std::cout<<"\n$$$$$$$$$$$$$$$$$$$yes$$$$$$$$$$$$$$$$$$$$$$\n";*/

               //std::cout<<std::endl<<c++<<". Varname: "<<varName;

                if (varName.compare("instrument_name") == 0)
                {
                    if (cptrMetrics_i->source_is_instrument())
                    {
                        m_strsKeys["${instrument_name}"] = cptrMetrics_i->instrument_name();
                    }
                    else
                    {
                        m_strsKeys["${instrument_name}"] = "Instrument All";

                    }
                }
                else if (varName.compare("strategy_name") == 0)
                {
                    /*if (cptrMetrics_i->source_is_instrument())
                    {
                    if (cptrStrategyMetrics_i != NULL)
                    {
                    m_strsKeys["strategy_name"] = cptrStrategyMetrics_i->strategy_name();
                    }
                    else
                    {
                    m_strsKeys["strategy_name"] = "Error";
                    }

                    }
                    else
                    {*/
                    m_strsKeys["${strategy_name}"] = cptrMetrics_i->strategy_name();


                }
                else
                {
                    tsa::string_splitter stringSplitter;
                    if (varName == "<if-cond>")
                    {
                        std::cout << "\nCondition\n";
                    }
                    size_t nNumOfSubStrings = stringSplitter.split(varName, '|');
                    if (nNumOfSubStrings != 3)
                    {
                        std::cout << "\nINFO: Widget mode\n";

                        if (nNumOfSubStrings == 2)
                        {
                            if (stringSplitter[1].str() == "widget")
                            {
                                continue;
                            }
                            else
                            {
                                std::cout << "\nERROR: Unknown type\n";

                            }
                        }


                    }
                    else
                    {
                        // Split the varName
                        // the varName will be in the format varName|type1|type2
                        // type1 will be all/long/short
                        // type2 will be ccy/num/pct

                        // nNumOfSubSTrings should be 3 for the time being
                        //for (int i = 0; i < 3; i++)

                        try
                        {

                            std::string strColumnName = stringSplitter[0].str();
                            std::string strColoumnType1 = stringSplitter[1].str();
                            std::string strColoumnType2 = stringSplitter[2].str();

                            tsa::trade_grouping tradeGrouping = tsa::all_trades;

                            if (strColoumnType1.compare("all") == 0)
                            {
                                tradeGrouping = tsa::all_trades;
                            }
                            else if (strColoumnType1.compare("long") == 0)
                            {
                                tradeGrouping = tsa::long_only;
                            }
                            else if (strColoumnType1.compare("short") == 0)
                            {
                                tradeGrouping = tsa::short_only;
                            }


                            //if(strColumnName.compare("avg_transaction_size") != 0 )
                            //if(true)
                            try
                            {

                                tsa::variant variantValue;


                                if (!cptrMetrics_i->exists(strColumnName, tradeGrouping))
                                {
                                    std::cout << "\nERROR: Column not found: " << strColumnName << "\n";
                                    m_strsKeys[key.first] = "Error";
                                    continue;
                                }

                                variantValue = cptrMetrics_i->get_metric(strColumnName, tradeGrouping);



                                if (variantValue.defined())
                                {
                                    //std::cout << "\n" << varName << " : " << variantValue.to_float();
                                    double fValue = 0;
                                    int64_t nValue = 0;
                                    //char* cValue = new char[10];
                                    ////sprintf(cValue, "%f", fValue);
                                    ////result.write(cValue,10);
                                    //free(cValue);
                                    std::string strValue;// = boost::lexical_cast<std::string>(fValue);


                                    if (strColoumnType2.compare("ccy") == 0)
                                    {
                                        fValue = variantValue.get<double>();
                                        strValue = tsa::string_util::to_string_money(fValue, 2, true);
                                    }
                                    else if (strColoumnType2.compare("int64") == 0)
                                    {
                                        nValue = variantValue.get<int64_t>();
                                        strValue = std::to_string(nValue);

                                    }
                                    else if (strColoumnType2.compare("double_1") == 0)
                                    {
                                        fValue = variantValue.get<double>();
                                        strValue = tsa::string_util::to_string__precision(fValue, 1);
                                    }
                                    else if (strColoumnType2.compare("percent") == 0)
                                    {
                                        fValue = variantValue.get<double>();
                                        strValue = tsa::string_util::to_string__precision(fValue, 1) + "%";
                                    }
                                    else if (strColoumnType2.compare("double_2") == 0)
                                    {
                                        fValue = variantValue.get<double>();
                                        strValue = tsa::string_util::to_string__precision(fValue, 2);
                                    }
                                    else if (strColoumnType2.compare("double_4") == 0)
                                    {
                                        fValue = variantValue.get<double>();
                                        strValue = tsa::string_util::to_string__precision(fValue, 4);
                                    }
                                    else if (strColoumnType2.compare("bool") == 0)
                                    {
                                        bool bValue = variantValue.get<bool>();
                                        if (bValue)
                                            strValue = "true";
                                        else
                                            strValue = "false";
                                    }
                                    else if (strColoumnType2.compare("duration") == 0)
                                    {
                                        strValue = variantValue.get<tsa::duration>().to_string();
                                        //strValue = std::to_string(bValue);
                                    }
                                    else if (strColoumnType2.compare("duration_short") == 0)
                                    {
                                        strValue = variantValue.get<tsa::duration>().to_string__concise();
                                        //strValue = std::to_string(bValue);
                                    }


                                    else if (strColoumnType2.compare("datetime") == 0)
                                    {
                                        strValue = variantValue.get<tsa::date_time>().to_string__report();
                                    }
                                    else if (strColoumnType2.compare("string") == 0)
                                    {
                                        strValue = variantValue.get<std::string>();
                                    }
                                    else
                                    {
                                        strValue = "Error";
                                    }

                                    if (fValue < 0)
                                    {
                                        strValue = "<span class='NegativeNumber'>" + strValue + "</span>";
                                    }

                                    //if (strValue.size() >= 20) strValue = "Junk";

                                    m_strsKeys[key.first] = strValue;
                                    //qDebug(strValue.c_str());


                                }
                                else
                                {
                                    std::cout << "\nERROR: Variant not defined: " << strColumnName << "\n";
                                    m_strsKeys[key.first] = "NA";
                                }
                            }
                            catch (...)
                            {
                                std::cout << "\nERROR: Column not found: " << strColumnName << "\n";
                                m_strsKeys[key.first] = "Error";

                            }
                        }

                        catch (...)
                        {
                            std::cout << "\nError in metrics read\n";
                            m_strsKeys[key.first] = "NA";

                        }
                    }
                }
                //int gross_profit_int = m.get_metric(tsa::metric::gross_profit, tsa::all_trades).to_int();
            }
        }
        catch (...)
        {
            std::cout << "\nError in metrics read\n";

        }

       // std::cout<<"\n\n\n\nAfter\n\n\n\n\n"<<template_body<<"\n\n\n\n"<<std::flush;


    }


}
