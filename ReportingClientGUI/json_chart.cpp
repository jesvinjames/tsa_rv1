#include "json_chart.h"



json_chart::json_chart()
{
    m_json.insert("type", "stock");
    m_json.insert("theme", "light");
    m_json.insert("categoryAxesSettings", tsa::json::object{});
    m_json.insert("valueAxesSettings", tsa::json::object{});
    m_json.insert("chartScrollbarSettings", tsa::json::object{});
    m_json.insert("dataSets", tsa::json::array{});
    m_json.insert("dataDateFormat", "YYYY-MM-DDTJJ:NN:SS:FFF");
    m_json.insert("panels", tsa::json::array{});
    m_json.insert("chartCursorSettings", tsa::json::object{});
    m_json.insert("panelsSettings", tsa::json::object{});


    /*m_properties.chartdiv = "chartdiv";
    m_properties.type = "stock";
    m_properties.theme = "light";
    m_properties.categoryAxesSettings.autoGridCount = true;
    m_properties.categoryAxesSettings.equalSpacing = true;
    m_properties.categoryAxesSettings.startOnAxis = false;
    m_properties.categoryAxesSettings.minPeriod = "fff";
    m_properties.chartCursorSettings.categoryBalloonEnabled = true;
    m_properties.chartCursorSettings.valueBalloonsEnabled = true;
    m_properties.chartCursorSettings.categoryBalloonText = "[[category]]";
    m_properties.chartCursorSettings.cursorColor = "rgb(200,0,0)";
    m_properties.chartScrollbarSettings.enabled = false;
    m_properties.chartScrollbarSettings.height = 30;
    m_properties.chartScrollbarSettings.graphType = "line";
    m_properties.dataDateFormat = "YYYY-MM-DDTJJ:NN:SS:QQQ";
    m_properties.valueAxesSettings.autoGridCount = true;
    m_properties.valueAxesSettings.position = "left";*/

}


json_chart::~json_chart()
{
}

void json_chart::setChartTheme()
{
    m_json["theme"] =m_properties.theme;
}

void json_chart::setChartType()
{
    m_json["type"] =m_properties.type;
}

void json_chart::setDataDateFormat()
{
    m_json["dataDateFormat"] = m_properties.dataDateFormat;
}

void json_chart::setValueAxesSettings()
{
    m_json["valueAxesSettings"].insert("autoGridCount", m_properties.valueAxesSettings.autoGridCount);
    m_json["valueAxesSettings"].insert("position",m_properties.valueAxesSettings.position);
}

void json_chart::setChartScrollbarSettings()
{
    m_json["chartScrollbarSettings"].insert("enabled",m_properties.chartScrollbarSettings.enabled);
}



void json_chart::setCategoryAxesSettings()
{
    m_json["categoryAxesSettings"].insert("autoGridCount", m_properties.categoryAxesSettings.autoGridCount);
    m_json["categoryAxesSettings"].insert("equalSpacing", m_properties.categoryAxesSettings.equalSpacing);
    m_json["categoryAxesSettings"].insert("startOnAxis", m_properties.categoryAxesSettings.startOnAxis);

    m_json["categoryAxesSettings"].insert("minPeriod", m_properties.categoryAxesSettings.minPeriod);
    m_json["categoryAxesSettings"].insert("groupToPeriods", tsa::json::array{});
    m_json["categoryAxesSettings"]["groupToPeriods"].push_back("fff");
    m_json["categoryAxesSettings"].insert("dateFormats", tsa::json::array{});
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][0].insert("period", "fff");
    m_json["categoryAxesSettings"]["dateFormats"][0].insert("format", "JJ:NN:SS:QQQ");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][1].insert("period", "ss");
    m_json["categoryAxesSettings"]["dateFormats"][1].insert("format", "JJ:NN:SS:QQQ");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][2].insert("period", "mm");
    m_json["categoryAxesSettings"]["dateFormats"][2].insert("format", "JJ:NN:SS:QQQ");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][3].insert("period", "hh");
    m_json["categoryAxesSettings"]["dateFormats"][3].insert("format", "JJ:NN:SS:QQQ");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][4].insert("period", "DD");
    m_json["categoryAxesSettings"]["dateFormats"][4].insert("format", "YYYY-MM-DD");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][5].insert("period", "MM");
    m_json["categoryAxesSettings"]["dateFormats"][5].insert("format", "YYYY-MM-DD");
    m_json["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    m_json["categoryAxesSettings"]["dateFormats"][6].insert("period", "YYYY");
    m_json["categoryAxesSettings"]["dateFormats"][6].insert("format", "YYYY-MM-DD");
}

void json_chart::setChartCursorSettings()
{
    m_json["chartCursorSettings"].insert("valueBalloonsEnabled",m_properties.chartCursorSettings.valueBalloonsEnabled);
    m_json["chartCursorSettings"].insert("valueLineBalloonEnabled", m_properties.chartCursorSettings.valueLineBalloonEnabled);
    m_json["chartCursorSettings"].insert("valueLineAlpha", m_properties.chartCursorSettings.valueLineAlpha);
    m_json["chartCursorSettings"].insert("valueLineEnabled", m_properties.chartCursorSettings.valueLineEnabled);
    m_json["chartCursorSettings"].insert("zoomable", m_properties.chartCursorSettings.zoomable);
    m_json["chartCursorSettings"].insert("categoryBalloonEnabled",m_properties.chartCursorSettings.categoryBalloonEnabled);
    m_json["chartCursorSettings"].insert("categoryBalloonText",m_properties.chartCursorSettings.categoryBalloonText);
    m_json["chartCursorSettings"].insert("categoryBalloonDateFormats", tsa::json::array{});
    m_json["chartCursorSettings"]["categoryBalloonDateFormats"].push_back(tsa::json::object{});
    m_json["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("period", "fff");
    m_json["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("format", "YYYY-MM-DD JJ:NN:SS:QQQ");
    m_json["chartCursorSettings"].insert("cursorColor",m_properties.chartCursorSettings.cursorColor);
}

void json_chart::setPanelsSettings()
{
    m_json["panelsSettings"].insert("precision",6);
}
void json_chart::setChartDiv(const std::string& chartdiv)
{
    m_properties.chartdiv = chartdiv;
}

std::string json_chart::get_price_chart_string(const tsc::report_chart_manager& chart_man, const tsa::record_set& data_ms)
{
    /*tsc::report_chart_manager chart_man;
    tsa::os::path logDbPath = strategyPath / "log_db";

    tsa::fast::database dataBase(logDbPath, tsa::no_create_db);
    tsa::record_set rset;
    std::filebuf fb;
    fb.open("test.txt", std::ios::out);
    std::ostream os(&fb);
    size_t FPPrecision = 8;
    size_t maxNumRec = tsa::print_all;
    dataBase.print_table("instrument_0_trade_log",os, FPPrecision,
        maxNumRec,true);
    dataBase.series_load("instrument_0_trade_log", rset, 0, 100);//.mem_table("instrument_0_transaction_log");
    size_t rec10_count = rset.size();
    size_t col10_count = rset.column_count();
    for (int col=0; col < col10_count; col++)
    {
        std::cout << rset[0][col] <<"\n";
    }
    chart_man.select_chart(strategyPath,0, dataBase);
    */
    setChart_DataSets(data_ms);
    setChart_Panels(chart_man);
    setCategoryAxesSettings();
    setChartCursorSettings();
    setDataDateFormat();
    setPanelsSettings();
    setChartScrollbarSettings();
    setValueAxesSettings();
    std::string equityJsonString = m_json.to_string();


    std::string rv;
        rv.reserve(equityJsonString.size() + 10000);
        rv += "AmCharts.makeChart(\"";
        rv += m_properties.chartdiv + "\"," + equityJsonString + ");";

        /*std::stringstream equityJsonStream;
        equityJsonStream << "<style>\n#chartdiv{\nwidth: 100 % ;height:100%;}\n</style>\n<!--Resources-->\n<script src =\"amcharts.js\"></script>\n<script src = \"serial.js\"></script>\n";
        equityJsonStream << "<script src=\"amstock.js\"></script>\n<script src =\"plugins/export/export.min.js\"></script>\n";
        equityJsonStream << "<link rel = \"stylesheet\" href =\"plugins/export/export.css\"type = \"text / css\" media = \"all\" / ><script src = \"themes / light.js\"></script>";
        equityJsonStream << "<script>var chart = AmCharts.makeChart(\"" << m_properties.chartdiv << "\"," << equityJsonString << ");";
        equityJsonStream << "</script><!--HTML--><div id = \"chartdiv\"></div>";
        equityJsonString = equityJsonStream.str();*/
        return rv;
}

void json_chart::setChart_DataSets(const tsa::record_set& data_ms)
{
    m_json["dataSets"].push_back(tsa::json::object{});
    m_json["dataSets"][0].insert("categoryField", "price_Date");
    m_json["dataSets"][0].insert("dataProvider", tsa::json::array{});
    m_json["dataSets"][0].insert("fieldMappings", tsa::json::array{});
    //chart_man.set_range(1, 200);
//	chart_man.prepare_chart_data_for_web_charts();
    //const tsa::record_set& data_ms = chart_man.chart_data__millisec();
    size_t rec_count = data_ms.size();
    size_t col_count = data_ms.column_count();
    std::vector<std::string> column_names;
    for (int i = 0; i < col_count; i++)
    {
        std::stringstream ss;
        ss << i;
        std::string str = ss.str();
        column_names.push_back(str);
        m_json["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
        m_json["dataSets"][0]["fieldMappings"][i].insert("fromField", str);
        m_json["dataSets"][0]["fieldMappings"][i].insert("toField", str);
    }
    /*priceChartJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][1].insert("fromField", "high");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][1].insert("toField", "high");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][2].insert("fromField", "low");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][2].insert("toField", "low");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][3].insert("fromField", "close");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][3].insert("toField", "close");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][4].insert("fromField", "volume");
    priceChartJsonFormat["dataSets"][0]["fieldMappings"][4].insert("toField", "volume");
    //equityJsonData[0][0].get<double>();
    //size_t col_equity = equity_data.column_position("cum_equity");*/
    auto dataProviderArray = m_json["dataSets"][0]["dataProvider"];
    for (int r = 0; r < rec_count; r++)
    {
        tsa::json jn = tsa::json::object{};
        jn.insert("price_Date", data_ms.timestamp(r).to_string__javascript_format());
        for (int c = 0; c < col_count; c++)
            //m_cEquityChart->add(equity_data.timestamp(i), (double)equity_data[i][0]);
            jn.insert(column_names[c], data_ms[r][c].as<std::string>());
        dataProviderArray.push_back(jn);
    }
}
void json_chart::setChart_Panels(const tsc::report_chart_manager& chart_man)
{
    const tsa::chart_def& ch_def = chart_man.chart_def();
    size_t panel_count = ch_def.m_pane_meta_infos.size();

    for (int p = 0; p < panel_count; p++)
    {
        panel_set(ch_def.m_pane_meta_infos[p], p);
        std::vector<tsc::report_chart_manager::order_display_info> order_info = chart_man.order_display_infos_for_pane(p);
        size_t no_orders = order_info.size();
        m_json["panels"][p].insert("trendLines", tsa::json::array{});
        for (int count_order = 0; count_order < no_orders; count_order++)
        {
            plot_orders(order_info[count_order], p, count_order);
        }
    }

}



void json_chart::panel_set(const tsa::pane_and_plot_meta_data& _pane_plot_meta, size_t _panel_pos)
{
    m_json["panels"].push_back(tsa::json::object{});
    uint64_t pane_height_pct = _pane_plot_meta.m_pane_props[tsa::chart_property::height].get<uint64_t>();
    std::string panel_title = _pane_plot_meta.m_pane_props[tsa::chart_property::title].get<std::string>();

    //*********************
    //PETER NEEDS TO CHANGE THIS CODE !!!
    //********************
    int color_int = _pane_plot_meta.m_pane_props[tsa::chart_property::pane_bg_color].get<int>();
    tsa::color c; c.from_int(color_int);
    std::string color_pbg = c.to_rgb_string__for_amcharts();
    m_json["panels"][_panel_pos].insert("percentHeight", pane_height_pct);
    m_json["panels"][_panel_pos].insert("title",panel_title);
    m_json["panels"][_panel_pos].insert("stockLegend", tsa::json::object{});
    m_json["panels"][_panel_pos].insert("stockGraphs", tsa::json::array{});
    size_t num_plots = _pane_plot_meta.m_plot_props.size();
    size_t plot_pos = 0;
    for (size_t i = 0; i < num_plots; i++)
    {
        if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::ohlc)
        {
            plot_ohlc(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }

        else if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::line)
        {
            plot_line(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }
        else if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::bar)
        {
            plot_bar(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }
        else if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::area)
        {
            plot_Area(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }
        else if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::bullet_line)
        {
            plot_BulletLine(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }
        else if (_pane_plot_meta.m_plot_props[i][tsa::chart_property::type].get_string_ref()== tsa::chart_property_value::step_line)
        {
            plot_StepLine(_pane_plot_meta.m_plot_props[i], _panel_pos, plot_pos);
            plot_pos++;
        }
}

    size_t num_guides = _pane_plot_meta.m_h_guide_lines.size();
    if (num_guides)
    {
        m_json["panels"][_panel_pos].insert("guides", tsa::json::array{});
        for (size_t guide_pos = 0; guide_pos < num_guides; guide_pos++)
        {
            plot_GuideLines(_pane_plot_meta.m_h_guide_lines[guide_pos], _panel_pos, guide_pos);
        }
    }

}

void json_chart::plot_ohlc(const tsa::plot_properties& _plot_props, size_t _panel_pos, size_t _plot_pos)
{
    m_json["panels"][_panel_pos]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& plot_jsn = m_json["panels"][_panel_pos]["stockGraphs"][_plot_pos];
    plot_jsn.insert("fillAlphas", 0);
    plot_jsn.insert("balloonText", "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>");
    plot_jsn.insert("closeField", _plot_props[tsa::chart_property::data_column_3].as<std::string>());
    std::string color_prim_rgb = _plot_props.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();
    //tsa::json& plot_jsn = priceChartJsonFormat["panels"][countpanel]["stockGraphs"][countplot];
    insert_plot_name_if_exists(_plot_props, plot_jsn);

    plot_jsn.insert("fillColors", color_prim_rgb);
    plot_jsn.insert("highField", _plot_props[tsa::chart_property::data_column_1].as<std::string>());
    plot_jsn.insert("lineColor", color_prim_rgb);
    plot_jsn.insert("lineAlpha", 1);
    plot_jsn.insert("lowField", _plot_props[tsa::chart_property::data_column_2].as<std::string>());
    plot_jsn.insert("lineThickness", 3);
    plot_jsn.insert("useDataSetColors", false);
    plot_jsn.insert("negativeFillColors", color_prim_rgb);
    plot_jsn.insert("negativeLineColor", color_prim_rgb);
    plot_jsn.insert("openField", _plot_props[tsa::chart_property::data_column_0].as<std::string>());
    plot_jsn.insert("type", _plot_props[tsa::chart_property::type].get_string_ref());
    plot_jsn.insert("valueField", _plot_props[tsa::chart_property::data_column_3].as<std::string>());

}

void json_chart::plot_line(const tsa::plot_properties& plotinfo, size_t countpanel, size_t countplot)
{
    m_json["panels"][countpanel]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][countpanel]["stockGraphs"][countplot];
    std::string color_prim_rgb = plotinfo.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();

    int line_thickness = plotinfo[tsa::chart_property::weight].as<int>();
    line_thickness = line_thickness > 10 ? 1 : line_thickness;
    insert_plot_name_if_exists(plotinfo, j);
    j.insert("fillAlphas", 0);
    j.insert("showBalloon", false);
    j.insert("lineColor", color_prim_rgb);
    j.insert("useDataSetColors", false);
    j.insert("lineThickness", line_thickness);
    j.insert("valueField", plotinfo[tsa::chart_property::data_column_0].as<std::string>());
    j.insert("type", plotinfo[tsa::chart_property::type].get_string_ref());


}

void json_chart::plot_bar(const tsa::plot_properties & plotinfo, size_t countpanel, size_t countplot)
{

    m_json["panels"][countpanel]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][countpanel]["stockGraphs"][countplot];
    std::string color_prim_rgb = plotinfo.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();
    insert_plot_name_if_exists(plotinfo, j);
    j.insert("fillAlphas", 1);
    j.insert("showBalloon", false);
    j.insert("fillColors", color_prim_rgb);
    j.insert("useDataSetColors", false);
    j.insert("connect", true);
    j.insert("valueField", plotinfo[tsa::chart_property::data_column_0].as<std::string>());
    j.insert("type","column");

    //priceChartJsonFormat["panels"][countpanel]["stockGraphs"][countplot] = j;


}

void json_chart::plot_Area(const tsa::plot_properties & plotinfo, size_t countpanel, size_t countplot)
{
    m_json["panels"][countpanel]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][countpanel]["stockGraphs"][countplot];
    std::string color_prim_rgb = plotinfo.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();
    insert_plot_name_if_exists(plotinfo, j);
    j.insert("fillAlphas", 1);
    j.insert("showBalloon",false);
    j.insert("fillColors", color_prim_rgb);
    j.insert("negativeBase", 0);
    j.insert("useDataSetColors", false);
    j.insert("valueField",plotinfo[tsa::chart_property::data_column_0].as<std::string>());
    j.insert("type", "line");
}

void json_chart::plot_BulletLine(const tsa::plot_properties& plotinfo, size_t panel_pos, size_t plot_pos)
{
    m_json["panels"][panel_pos]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][panel_pos]["stockGraphs"][plot_pos];
    std::string color_prim_rgb = plotinfo.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();
    insert_plot_name_if_exists(plotinfo, j);
    j.insert("forceGap",true);
    j.insert("gapPeriod", 1);
    j.insert("showBalloon", false);
    j.insert("lineColor", color_prim_rgb);
    j.insert("useDataSetColors", false);
    j.insert("bullet","round");
    j.insert("bulletSize", 6);
    j.insert("valueField", plotinfo[tsa::chart_property::data_column_0].as<std::string>());
    j.insert("type", "line");
}

void json_chart::plot_StepLine(const tsa::plot_properties& plotinfo, size_t panel_pos, size_t plot_pos)
{
    m_json["panels"][panel_pos]["stockGraphs"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][panel_pos]["stockGraphs"][plot_pos];
    std::string color_prim_rgb = plotinfo.get_color(tsa::chart_property::color_primary).to_rgb_string__for_amcharts();
    int line_thickness = plotinfo[tsa::chart_property::weight].as<int>();
    line_thickness = line_thickness > 10 ? 1 : line_thickness;


    j.insert("fillAlphas", 0);
    insert_plot_name_if_exists(plotinfo,j);
    j.insert("showBalloon", false);
    j.insert("lineColor", color_prim_rgb);
    j.insert("useDataSetColors", false);
    j.insert("lineThickness", line_thickness);
    j.insert("valueField", plotinfo[tsa::chart_property::data_column_0].as<std::string>());
    j.insert("type","step");
}

void json_chart::plot_GuideLines(const tsa::horiz_plot_guide_line& guide_info, size_t panel_pos, size_t guid_pos)
{	m_json["panels"][panel_pos]["guides"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][panel_pos]["guides"][guid_pos];
    int line_thickness = guide_info.weight;
    line_thickness = line_thickness > 10 ? 1 : line_thickness;
    j.insert("above",true);
    j.insert("value", guide_info.y_value);
    j.insert("lineThickness",line_thickness);
    j.insert("lineColor", guide_info.line_color.to_rgb_string__for_amcharts());
    j.insert("useDataSetColors", false);
    j.insert("label", guide_info.y_value);
    j.insert("lineAlpha",1);
    j.insert("inside",false);

}



void json_chart::insert_plot_name_if_exists(const tsa::plot_properties& _props, tsa::json& _json) {
    bool panel_has_title = _props.property_exists(tsa::chart_property::name);
    if (panel_has_title) {
        _json.insert("title", _props[tsa::chart_property::name].get_string_ref());
    }
}

void json_chart::plot_orders(const tsc::report_chart_manager::order_display_info& orders, size_t panel_pos,size_t order_pos)
{
    m_json["panels"][panel_pos]["trendLines"].push_back(tsa::json::object{});
    tsa::json& j = m_json["panels"][panel_pos]["trendLines"][order_pos];
    std::stringstream popup_text;
    popup_text<< "order_id:<b>"<<orders.id<<"<br>instrument_id:" << orders.instr_id<<"<br>action:"<< tsa::order::to_string(orders.action) <<"<br>type:"<<tsa::order::to_string(orders.type)<<"<br>price:"<<orders.price<<"<br>quantity:"<<orders.quantity;

    j.insert("initialValue",orders.price);
    j.insert("initialDate",orders.display_begin_timestamp);
    j.insert("finalValue",orders.price);
    j.insert("finalDate", orders.display_end_timestamp);
    std::string ord_color;
    if (orders.type == tsa::order::type::limit)
    {
        ord_color = "#0000FF";
    }
    else if (orders.type == tsa::order::type::stop)
    {
        ord_color = "#c00";
    }
    else if (orders.type == tsa::order::type::market)
    {
        ord_color = "#0c0";
    }
    j.insert("lineColor",ord_color);
    if(orders.display_line)
    {
        j.insert("lineThickness", 1);
        j.insert("balloonText", popup_text.str());
    }
    else {
        j.insert("lineThickness", 0);
    }
    if (orders.display_symbol)
    {
        tsa::json order_img = tsa::json::object{};


        order_img.insert("svgPath", "M0,0 L0,2 L2,1 Z");
        order_img.insert("width", 10);
        order_img.insert("height", 10);
        order_img.insert("offsetX", 5);
        order_img.insert("balloonText", popup_text.str());
        order_img.insert("balloonColor", ord_color);
        if (orders.action == tsa::order::action::buy)
        {
            order_img.insert("rotation", 270);
            order_img.insert("offsetY", 6);
        }
        else if (orders.action == tsa::order::action::sell)
        {
            order_img.insert("rotation",90);
            order_img.insert("offsetY", -5);
        }
        order_img.insert("color",ord_color);
        j.insert("initialImage", order_img);
    }
    else
    {
    }


}



/*std::string json_chart::to_json_string()
{
    setCategoryAxesSettings();
    setChartCursorSettings();
    setDataDateFormat();
    setPanelsSettings();
    setChartScrollbarSettings();
    setValueAxesSettings();
    std::string equityJsonString = m_json.to_string();


    std::string rv;
    rv.reserve(equityJsonString.size() + 10000);
    rv += "<style>\n#chartdiv{\nwidth: 100 % ;height: 900px;}</style>\n"
        "<script src =\"amcharts.js\"></script>\n"
        "<script src = \"serial.js\"></script>\n"
        "<script src=\"amstock.js\"></script>\n"
        "<script src =\"plugins/export/export.min.js\"></script>\n";

    rv += "<link rel = \"stylesheet\" href =\"plugins/export/export.css\"type = \"text / css\" media = \"all\" / >"
        "<script src = \"themes/light.js\"></script>";
    rv += "<script>var chart = AmCharts.makeChart(\"";
    rv += m_properties.chartdiv + "\"," + equityJsonString + ");";
    rv += "</script><!--HTML--><div id = \"chartdiv\"></div>";

    /*std::stringstream equityJsonStream;
    equityJsonStream << "<style>\n#chartdiv{\nwidth: 100 % ;height:100%;}\n</style>\n<!--Resources-->\n<script src =\"amcharts.js\"></script>\n<script src = \"serial.js\"></script>\n";
    equityJsonStream << "<script src=\"amstock.js\"></script>\n<script src =\"plugins/export/export.min.js\"></script>\n";
    equityJsonStream << "<link rel = \"stylesheet\" href =\"plugins/export/export.css\"type = \"text / css\" media = \"all\" / ><script src = \"themes / light.js\"></script>";
    equityJsonStream << "<script>var chart = AmCharts.makeChart(\"" << m_properties.chartdiv << "\"," << equityJsonString << ");";
    equityJsonStream << "</script><!--HTML--><div id = \"chartdiv\"></div>";
    equityJsonString = equityJsonStream.str();
    return rv;


}*/



