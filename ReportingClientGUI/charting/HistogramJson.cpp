#include "HistogramJson.h"



HistogramJson::HistogramJson()
{
	histogramjsonFormat.insert("type", "serial");
	histogramjsonFormat.insert("theme", "light");
	histogramjsonFormat.insert("allLabels", tsa::json::array{});
	histogramjsonFormat.insert("dataProvider", tsa::json::array{});
	histogramjsonFormat.insert("valueAxes", tsa::json::array{});
	histogramjsonFormat.insert("startDuration", 1);
	histogramjsonFormat.insert("graphs", tsa::json::array{});
	histogramjsonFormat.insert("chartCursor", tsa::json::object{});
	histogramjsonFormat.insert("categoryField", "stddev_Value");
	histogramjsonFormat.insert("categoryAxis", tsa::json::object{});
	histogramjsonFormat.insert("export", tsa::json::object{});
	histogramChartContents.chartdiv = "trade_histogram_chart";
	histogramChartContents.type = "serial";
	histogramChartContents.theme = "light";
//	histogramChartContents.marginRight = 70;
	histogramChartContents.valueAxes.axisAlpha = 1;
	histogramChartContents.valueAxes.position = "left";
	histogramChartContents.valueAxes.gridalpha = 0;
	histogramChartContents.startDuration = 0;
	histogramChartContents.allLabels.text = "";
	histogramChartContents.allLabels.align = "center";
	histogramChartContents.allLabels.bold = true;
	histogramChartContents.allLabels.size = 15;
	histogramChartContents.allLabels.y = 10;
	histogramChartContents.graphs.balloonText = "<b>[[stddev_Value]]</b>";
	histogramChartContents.graphs.fillColorsField = "color_Value";
	histogramChartContents.graphs.fillAlphas = 1;
	histogramChartContents.graphs.lineAlpha = 0.2;
	histogramChartContents.graphs.lineColor = "FFFFFF";
	histogramChartContents.graphs.type = "column";
	histogramChartContents.graphs.columnWidth = 0.8;
	histogramChartContents.graphs.valueField = "count_Value";
	histogramChartContents.chartCursor.categoryBalloonEnabled = false;
	histogramChartContents.chartCursor.cursorAlpha = 0;
	histogramChartContents.chartCursor.zoomable = false;
	histogramChartContents.categoryField = "stddev_Value";
	histogramChartContents.categoryAxis.autoGridCount = true;
	histogramChartContents.categoryAxis.gridAlpha = 0;
	histogramChartContents.categoryAxis.tickLength = 0;
	histogramChartContents.categoryAxis.gridPosition = "start";
	histogramChartContents.categoryAxis.labelsEnabled = false;
	histogramChartContents.exporte.enabled = false;
}
void HistogramJson::setChart_Type()
{
	histogramjsonFormat["type"] = histogramChartContents.type;
}
void HistogramJson::setChart_Theme()
{
	histogramjsonFormat["theme"] = histogramChartContents.theme;
}
void HistogramJson::setChart_DataProvider(std::vector<HistogramJsonData::ChartRangeBuckets> &histogramChartData)
{
	std::vector<HistogramJsonData::ChartRangeBuckets>::const_iterator iterator_HistogramData;
	auto dataProviderArray = histogramjsonFormat["dataProvider"];
	for (iterator_HistogramData = histogramChartData.begin(); iterator_HistogramData != histogramChartData.end(); iterator_HistogramData++)
	{
		dataProviderArray.push_back(tsa::json::object{ {"stddev_Value",(*iterator_HistogramData).stddevValue_OfRangeBucket},{"count_Value",(*iterator_HistogramData).count_OfRangeBucket},{"color_Value",(*iterator_HistogramData).color_OfRangeBucket} });
	}
}
void HistogramJson::setChart_ValueAxes()
{
	histogramjsonFormat["valueAxes"].push_back(tsa::json::object{ { "axisAlpha" ,histogramChartContents.valueAxes.axisAlpha},{ "position" ,histogramChartContents.valueAxes.position},{ "gridAlpha" ,histogramChartContents.valueAxes.gridalpha} });
}
void HistogramJson::setChart_AllLabels()
{
	histogramjsonFormat["allLabels"].push_back(tsa::json::object{ { "text" ,histogramChartContents.allLabels.text },{ "align",histogramChartContents.allLabels.align },
	{ "bold",histogramChartContents.allLabels.bold},{ "size" ,histogramChartContents.allLabels.size },{ "y" ,histogramChartContents.allLabels.y } });
}
void HistogramJson::setChart_StartDuration()
{
	histogramjsonFormat["startDuration"] = histogramChartContents.startDuration;
}
void HistogramJson::setChart_Graphs()
{
	histogramjsonFormat["graphs"].push_back(tsa::json::object{ { "balloonText" ,histogramChartContents.graphs.balloonText},{ "fillColorsField",histogramChartContents.graphs.fillColorsField},
	{ "fillAlphas",histogramChartContents.graphs.fillAlphas},{ "lineAlpha" ,histogramChartContents.graphs.lineAlpha},{ "type" ,histogramChartContents.graphs.type},
	{ "columnWidth" ,histogramChartContents.graphs.columnWidth},{"lineColor",histogramChartContents.graphs.lineColor}, { "valueField" ,histogramChartContents.graphs.valueField} });
}
void HistogramJson::setChart_ChartCursor()
{
	histogramjsonFormat["chartCursor"].insert("categoryBalloonEnabled", histogramChartContents.chartCursor.categoryBalloonEnabled);
	histogramjsonFormat["chartCursor"].insert("cursorAlpha", histogramChartContents.chartCursor.cursorAlpha);
	histogramjsonFormat["chartCursor"].insert("zoomable", histogramChartContents.chartCursor.zoomable);

}
void HistogramJson::setChart_CategoryField()
{
	histogramjsonFormat["categoryField"] = histogramChartContents.categoryField;
}
void  HistogramJson::setChart_categoryAxis(std::vector<HistogramJsonData::ChartLineList> &histogramLineData)
{
	std::vector<HistogramJsonData::ChartLineList>::const_iterator iterator_HistogramLineData;
	histogramjsonFormat["categoryAxis"].insert("autoGridCount", histogramChartContents.categoryAxis.autoGridCount);
	histogramjsonFormat["categoryAxis"].insert("gridAlpha", histogramChartContents.categoryAxis.gridAlpha);
	histogramjsonFormat["categoryAxis"].insert("tickLength", histogramChartContents.categoryAxis.tickLength);
	histogramjsonFormat["categoryAxis"].insert("gridPosition", histogramChartContents.categoryAxis.gridPosition);
	histogramjsonFormat["categoryAxis"].insert("labelsEnabled", histogramChartContents.categoryAxis.labelsEnabled);
	histogramjsonFormat["categoryAxis"].insert("guides", tsa::json::array{});
	auto guidesLinesArray = histogramjsonFormat["categoryAxis"]["guides"];
	for (iterator_HistogramLineData = histogramLineData.begin(); iterator_HistogramLineData !=histogramLineData.end()-1;iterator_HistogramLineData++)
	{
			guidesLinesArray.push_back(tsa::json::object{ { "category",(*iterator_HistogramLineData).lineStddevValue},{ "expand" , false},{ "lineColor","#228B22" },{ "lineAlpha" , 0.5 },{"position","top"},{"above",true}, {"label",(*iterator_HistogramLineData).linestddev} });
			guidesLinesArray.push_back(tsa::json::object{ { "category",(*iterator_HistogramLineData).lineStddevValue },{ "expand" , false },{ "lineColor","#228B22" },{ "lineAlpha" , 0 },{"position","bottom"},{ "above",true },{ "label",(*iterator_HistogramLineData).lineStddevValue } });
	}
	iterator_HistogramLineData = histogramLineData.end() - 1;
	std::stringstream meanValueStream;
	meanValueStream <<"\n"<< "Average" <<"\n"<< "(" << (*iterator_HistogramLineData).lineStddevValue << ")";
	std::string meanValuestring = meanValueStream.str();
	guidesLinesArray.push_back(tsa::json::object{ { "category",(*iterator_HistogramLineData).lineStddevValue},{ "expand" , false },{ "lineColor","#228B22" },{ "lineAlpha" , 0.5 },{ "position","top" },{ "above",true },{ "label","" } });
	guidesLinesArray.push_back(tsa::json::object{ { "category",(*iterator_HistogramLineData).lineStddevValue },{ "expand" , false},{ "lineColor","#FFFFFF" },{ "lineAlpha" , 0 },{ "position","bottom" },{ "above",true },{ "label",meanValuestring} });
		
}
void HistogramJson::setChart_Export()
{
	histogramjsonFormat["export"].insert("enabled",histogramChartContents.exporte.enabled);
}
std::string HistogramJson::getHistogramJson(std::vector<HistogramJsonData::ChartRangeBuckets> &histogramChartData, std::vector<HistogramJsonData::ChartLineList> &histogramLineData)
{
	this->setChart_Type();
	this->setChart_Theme();
	this->setChart_DataProvider(histogramChartData);
	this->setChart_ValueAxes();
	this->setChart_StartDuration();
	this->setChart_Graphs();
	this->setChart_AllLabels();
	this->setChart_ChartCursor();
	this->setChart_CategoryField();
	this->setChart_categoryAxis(histogramLineData);
	this->setChart_Export();
	std::string histogramJsonString = this->histogramjsonFormat.to_string();
	std::stringstream histogramJsonStream;
	histogramJsonStream << " AmCharts.makeChart(\"" << histogramChartContents.chartdiv << "\"," << histogramJsonString << ");";
	histogramJsonString = histogramJsonStream.str();

	return histogramJsonString;
}

HistogramJson::~HistogramJson()
{
}
