#include "HistogramJsonData.h"



HistogramJsonData::HistogramJsonData(double mean_Value,double stddev_Value)
{
	m_mean_OfValues = mean_Value;
	m_stddev_OfValues = stddev_Value;
}

HistogramJsonData::HistogramJsonData()
{
}


HistogramJsonData::~HistogramJsonData()
{
}
void HistogramJsonData::makeHistogramStddevBuckets()
{
	int chart_Stddev = -(m_no_OfChartStddevs);
	int no_of_stddevBuckets = (2 * m_no_OfChartStddevs) ;
	for (int i = 0; i < no_of_stddevBuckets; i++)
	{
			histogramStddevBuckets.low_OfStddevBucket = 0 + (chart_Stddev* m_stddev_OfValues);
			chart_Stddev++;
			histogramStddevBuckets.high_OfStddevBucket = 0 + (chart_Stddev*m_stddev_OfValues);
			histogramStddevRangeList.push_back(histogramStddevBuckets);
		//}
		/*else
		{
			histogramStddevBuckets.low_OfStddevBucket = m_mean_OfValues + (m_no_OfChartRanges*m_stddev_OfValues);
			m_no_OfChartRanges++;
			histogramStddevBuckets.high_OfStddevBucket = m_mean_OfValues+(m_no_OfChartRanges*m_stddev_OfValues);
			histogramStddevRangeList.push_back(histogramStddevBuckets);

		}*/
	}
}
void HistogramJsonData::makeHistogramRangeBuckets()
{
	double one_RangeValue =  (m_stddev_OfValues/m_no_OfChartRanges)  ;
	double RangeListValue = histogramStddevRangeList[0].low_OfStddevBucket;
	double counter_DataRangeList = (m_no_OfChartRanges* histogramStddevRangeList.size());
	for (int i = 0; i < counter_DataRangeList; i++)
	{
		histogramRangeBucket.low_OfRangeBucket = RangeListValue;
		RangeListValue =RangeListValue+one_RangeValue;
		histogramRangeBucket.high_OfRangeBucket = RangeListValue;
		histogramDataRangeList.push_back(histogramRangeBucket);
		
	}

/*	std::cout << one_RangeValue;
	for (int i = 0; i <counter; i++)
	{
		std::cout << histogramDataRangeList[i].low_OfRangeBucket << "\t" << histogramDataRangeList[i].high_OfRangeBucket << "\n";
	}*/
}
int HistogramJsonData::searchHistogramStddevBuckets(int beginingIndex,int endingIndex,double search_Value)
{


		int mid_Index;
		if (beginingIndex > endingIndex)
		{
			return -1;
		}
		else
		{
			mid_Index = (beginingIndex + endingIndex) / 2;
			if (search_Value < 0)
			{
				if (histogramStddevRangeList[mid_Index].low_OfStddevBucket <= search_Value&&histogramStddevRangeList[mid_Index].high_OfStddevBucket > search_Value)
				{
					return mid_Index;
				}
				else if (histogramStddevRangeList[mid_Index].low_OfStddevBucket > search_Value)
				{
					searchHistogramStddevBuckets(beginingIndex, mid_Index - 1, search_Value);
				}
				else if (histogramStddevRangeList[mid_Index].high_OfStddevBucket < search_Value)
				{
					searchHistogramStddevBuckets(mid_Index + 1, endingIndex, search_Value);
				}
			}
			else
			{
				if (histogramStddevRangeList[mid_Index].low_OfStddevBucket < search_Value&&histogramStddevRangeList[mid_Index].high_OfStddevBucket >= search_Value)
				{
					return mid_Index;
				}
				else if (histogramStddevRangeList[mid_Index].low_OfStddevBucket > search_Value)
				{
					searchHistogramStddevBuckets(beginingIndex, mid_Index - 1, search_Value);
				}
				else if (histogramStddevRangeList[mid_Index].high_OfStddevBucket < search_Value)
				{
					searchHistogramStddevBuckets(mid_Index + 1, endingIndex, search_Value);
				}

			}
		}

}
int HistogramJsonData::searchHistogramRangeBuckets(int beginingIndex, int endingIndex, double search_Value)
{

		int mid_Index;
		if (beginingIndex > endingIndex)
		{
			return -1;
		}
		else
		{
				mid_Index = (beginingIndex + endingIndex) / 2;
				if (search_Value < 0)
				{

					if (histogramDataRangeList[mid_Index].low_OfRangeBucket <= search_Value&&histogramDataRangeList[mid_Index].high_OfRangeBucket > search_Value)
					{
						histogramDataRangeList[mid_Index].count_OfRangeBucket++;
						histogramDataRangeList[mid_Index].color_OfRangeBucket = m_negative_Color;
						return mid_Index;
					}
					else if (histogramDataRangeList[mid_Index].low_OfRangeBucket > search_Value)
					{
						searchHistogramRangeBuckets(beginingIndex, mid_Index - 1, search_Value);
					}
					else if (histogramDataRangeList[mid_Index].high_OfRangeBucket < search_Value)
					{
						searchHistogramRangeBuckets(mid_Index + 1, endingIndex, search_Value);
					}
				}
				else
				{
					if (histogramDataRangeList[mid_Index].low_OfRangeBucket < search_Value && histogramDataRangeList[mid_Index].high_OfRangeBucket >= search_Value)
					{
						histogramDataRangeList[mid_Index].count_OfRangeBucket++;
						histogramDataRangeList[mid_Index].color_OfRangeBucket = m_positive_Color;
						return mid_Index;
					}
					else if (histogramDataRangeList[mid_Index].low_OfRangeBucket > search_Value)
					{
						searchHistogramRangeBuckets(beginingIndex, mid_Index - 1, search_Value);
					}
					else if (histogramDataRangeList[mid_Index].high_OfRangeBucket < search_Value)
					{
						searchHistogramRangeBuckets(mid_Index + 1, endingIndex, search_Value);
					}
				}
			}
}

void HistogramJsonData::makeHistogramOutlierBuckets(double outlierValue)
{
	int size_stddevRangeList = histogramStddevRangeList.size();
	if (outlierValue < histogramStddevRangeList[0].low_OfStddevBucket)
	{
		histogramNegativeOutlier.count_OfRangeBucket++;
		histogramNegativeOutlier.color_OfRangeBucket = "#FF6600";
	}
	else if(outlierValue > histogramStddevRangeList[size_stddevRangeList-1].high_OfStddevBucket)
	{
		histogramPositiveOutlier.count_OfRangeBucket++;
		histogramPositiveOutlier.color_OfRangeBucket = "#FF6600";
	}
}
void HistogramJsonData::set0_HistogramCountValue()
{
	std::vector<ChartRangeBuckets>::iterator iterator_DataRangeList;
	for (iterator_DataRangeList = histogramDataRangeList.begin(); iterator_DataRangeList != histogramDataRangeList.end(); ++iterator_DataRangeList)
	{
		(*iterator_DataRangeList).count_OfRangeBucket = 0;
	}
	histogramNegativeOutlier.count_OfRangeBucket = 0;
	histogramPositiveOutlier.count_OfRangeBucket = 0;

}
void HistogramJsonData::makeHistogramCustomLineValues()
{
	signed int chartStddevValue= -(m_no_OfChartStddevs);
	int counter_DataRangeList = histogramDataRangeList.size()-1;
	double histogramLineValue;
	int stddev_Index=1;
	double mean_OfRangeBucketValues;
	for (int indexDataRangeList = 1; indexDataRangeList <counter_DataRangeList ;indexDataRangeList++)
	{
		if (indexDataRangeList == stddev_Index)
		{
			if (chartStddevValue != 0)
			{
				histogramLineValue = (chartStddevValue*m_stddev_OfValues);
                histogramDataRangeList[indexDataRangeList].stddevValue_OfRangeBucket =tsa::string_util::to_string__precision(histogramLineValue, 2);
				chartLineData.linestddev = chartStddevValue;
                chartLineData.lineStddevValue =tsa::string_util::to_string__precision(histogramLineValue, 2);
				histogramCustomLineList.push_back(chartLineData);
			}
			else {
				stddev_Index--;
				mean_OfRangeBucketValues = (histogramDataRangeList[indexDataRangeList].high_OfRangeBucket + histogramDataRangeList[indexDataRangeList].low_OfRangeBucket) / 2;
				histogramDataRangeList[indexDataRangeList].stddevValue_OfRangeBucket = std::to_string(mean_OfRangeBucketValues);
			}
			chartStddevValue++;
			stddev_Index += m_no_OfChartRanges;
		}
		else {
			mean_OfRangeBucketValues = (histogramDataRangeList[indexDataRangeList].high_OfRangeBucket+histogramDataRangeList[indexDataRangeList].low_OfRangeBucket)/2;
			histogramDataRangeList[indexDataRangeList].stddevValue_OfRangeBucket = std::to_string(mean_OfRangeBucketValues);

		}
	}
	int intex_StddevRangeList =searchHistogramStddevBuckets(0,histogramStddevRangeList.size()-1,m_mean_OfValues);
	intex_StddevRangeList = m_no_OfChartRanges*intex_StddevRangeList;
		if (intex_StddevRangeList > 0 && intex_StddevRangeList < this->histogramDataRangeList.size())
		{
			int meanIndex = searchHistogramRangeBuckets(intex_StddevRangeList, intex_StddevRangeList + m_no_OfChartRanges, m_mean_OfValues);
            histogramDataRangeList[meanIndex].stddevValue_OfRangeBucket = tsa::string_util::to_string__precision(m_mean_OfValues, 2);

			histogramDataRangeList[meanIndex].count_OfRangeBucket--;
		}
    chartLineData.lineStddevValue = tsa::string_util::to_string__precision(m_mean_OfValues, 2);;
	histogramCustomLineList.push_back(chartLineData);

	

}
void::HistogramJsonData::get_HistogramJsonString(std::vector<double>&histogramDataValues)
{
	m_stddev_OfValues = tsa::vector_math::stdev_p(histogramDataValues);
	m_mean_OfValues = tsa::vector_math::average(histogramDataValues);
	if (m_stddev_OfValues == 0)
	{
		if (m_mean_OfValues < 0)
		{
			m_stddev_OfValues = (m_mean_OfValues+(m_mean_OfValues /2))*-1;
		}
		else if (m_mean_OfValues > 0)
		{
			m_stddev_OfValues = (m_mean_OfValues + (m_mean_OfValues / 2));
		}
	}
	this->makeHistogramStddevBuckets();
	this->makeHistogramRangeBuckets();
	this->set0_HistogramCountValue();
	int size_StddevRangeList = this->histogramStddevRangeList.size();
	std::vector<double>::const_iterator iterator_histogramDataValues;
	for (iterator_histogramDataValues = histogramDataValues.begin(); iterator_histogramDataValues != histogramDataValues.end(); ++iterator_histogramDataValues)
	{
		int intex_StddevBuckets = this->searchHistogramStddevBuckets(0, size_StddevRangeList - 1, (*iterator_histogramDataValues));
		if (intex_StddevBuckets == -1)
		{
			this->makeHistogramOutlierBuckets(*iterator_histogramDataValues);
		}
		else
		{
			int startIndex_RangeBuckets = intex_StddevBuckets * this->m_no_OfChartRanges;
			int endIntex_RangeBuckets = startIndex_RangeBuckets + this->m_no_OfChartRanges;
			int valueIntex_RangeList = this->searchHistogramRangeBuckets(startIndex_RangeBuckets,endIntex_RangeBuckets, (*iterator_histogramDataValues));
		}
	}
	this->histogramDataRangeList.push_back(this->histogramPositiveOutlier);
	this->histogramDataRangeList.insert(this->histogramDataRangeList.begin(), this->histogramNegativeOutlier);
	this->makeHistogramCustomLineValues();
}
