#pragma once
#include"HistogramJsonData.h"
#include<string>
#include<sstream>
#include<fstream>

class HistogramJson
{
private:

	tsa::json histogramjsonFormat = tsa::json::object{};
	struct chartContents {
		std::string chartdiv;
		std::string type;
		std::string theme;
	//	double marginRight;
		struct {
			double axisAlpha;
			std::string position;
			double gridalpha;
		}valueAxes;
		struct {
			std::string text;
			std::string	align;
			bool bold;
			double size;
			double y;
		}allLabels;
		double startDuration;
		struct {
			std::string balloonText;
			std::string fillColorsField;
			double fillAlphas;
			double	lineAlpha;
			std::string type;
			double columnWidth;
			std::string	valueField;
			std::string lineColor;
		}graphs;
		struct {
			bool categoryBalloonEnabled;
			double cursorAlpha;
			bool zoomable;
		}chartCursor;
		std::string categoryField;
		struct {
			bool autoGridCount;
			double gridAlpha;
			double tickLength;
			std::string gridPosition;
			bool labelsEnabled;
		}categoryAxis;
		struct {
			bool enabled;
		}exporte;
	};
public:
	chartContents histogramChartContents;


	HistogramJson();
	~HistogramJson();
	void setChart_Type();
	void setChart_Theme();
	void setChart_DataProvider(std::vector<HistogramJsonData::ChartRangeBuckets> &);
	void setChart_ValueAxes();
	void setChart_AllLabels();
	void setChart_StartDuration();
	void setChart_Graphs();
	void setChart_ChartCursor();
	void setChart_CategoryField();
	void setChart_categoryAxis(std::vector<HistogramJsonData::ChartLineList>&);
	void setChart_Export();
	std::string getHistogramJson(std::vector<HistogramJsonData::ChartRangeBuckets> &, std::vector<HistogramJsonData::ChartLineList> &);
	
};

