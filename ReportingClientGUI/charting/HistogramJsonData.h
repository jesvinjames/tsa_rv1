#pragma once
#include<iostream>
#include<vector>
#include<cmath>
#include<string>
#include<sstream>

#include"../tsa/util_string.h"
#include"../tsa/util_stats.h"
#include"../tsa/util_json.h"

class HistogramJsonData
{
public:
	double m_mean_OfValues;
	double m_stddev_OfValues;
	double m_no_OfChartRanges=5;
	 int m_no_OfChartStddevs=3;
	 std::string m_positive_Color = "rgb(121, 255, 121)";
	 std::string m_negative_Color = "rgb(255,121, 121)";
	struct ChartStddevBuckets {
		double low_OfStddevBucket;
		double high_OfStddevBucket;
	}histogramStddevBuckets;
	std::vector<struct ChartStddevBuckets>histogramStddevRangeList;
	struct ChartRangeBuckets {
		double low_OfRangeBucket;
		int count_OfRangeBucket;
		double high_OfRangeBucket;
		std::string color_OfRangeBucket;
		std::string stddevValue_OfRangeBucket;
	}histogramRangeBucket,histogramNegativeOutlier,histogramPositiveOutlier;
	std::vector<struct ChartRangeBuckets>histogramDataRangeList;
	struct ChartLineList {
		std::string lineStddevValue;
		int linestddev;
	}chartLineData;
	std::vector<struct ChartLineList>histogramCustomLineList;
	
	HistogramJsonData(double,double);
	HistogramJsonData();
	~HistogramJsonData();
	void makeHistogramStddevBuckets();
	void makeHistogramRangeBuckets();
	void set0_HistogramCountValue();
	int searchHistogramStddevBuckets(int,int,double);
	int searchHistogramRangeBuckets(int, int, double);
	void makeHistogramOutlierBuckets(double);
	void makeHistogramCustomLineValues();
	void get_HistogramJsonString(std::vector<double>&);

};

