#include "HistogramChart.h"



HistogramChart::HistogramChart()
{
}


HistogramChart::~HistogramChart()
{
}

std::string HistogramChart::getHistogramString(std::vector<double>&chartData, std::string strDivName_i)
{
	HistogramJsonData obj_HistogramJsonData;
	HistogramJson obj_HistogramJson;
	if (chartData.size() == 0)
	{
		obj_HistogramJson.histogramChartContents.allLabels.text = "Insufficient Data";
		chartData.push_back(1);
	}
	obj_HistogramJson.histogramChartContents.chartdiv = strDivName_i;
	obj_HistogramJsonData.get_HistogramJsonString(chartData);
	std::string string_HistogramChart = obj_HistogramJson.getHistogramJson(obj_HistogramJsonData.histogramDataRangeList,obj_HistogramJsonData.histogramCustomLineList);
	return string_HistogramChart;
}
