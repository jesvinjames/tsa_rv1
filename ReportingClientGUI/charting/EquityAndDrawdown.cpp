#include "EquityAndDrawdown.h"



EquityAndDrawdown::EquityAndDrawdown()
{
    EquityJsonFormat.insert("type", "stock");
    EquityJsonFormat.insert("theme", "light");
    EquityJsonFormat.insert("categoryAxesSettings", tsa::json::object{});
    EquityJsonFormat.insert("valueAxesSettings", tsa::json::object{});
    EquityJsonFormat.insert("chartScrollbarSettings", tsa::json::object{});
    EquityJsonFormat.insert("dataSets", tsa::json::array{});
    EquityJsonFormat.insert("dataDateFormat", "YYYY-MM-DDTJJ:NN:SS:QQQ");
    EquityJsonFormat.insert("panels", tsa::json::array{});
    EquityJsonFormat.insert("chartCursorSettings", tsa::json::object{});
    EquityJsonFormat.insert("panelsSettings", tsa::json::object{});
    /*equityChartContents.chartdiv = "chartdiv";
    equityChartContents.type = "stock";
    equityChartContents.theme = "light";
    equityChartContents.categoryAxesSettings.autoGridCount = true;
    equityChartContents.categoryAxesSettings.equalSpacing = true;
    equityChartContents.categoryAxesSettings.minPeriod = "fff";
    equityChartContents.categoryAxesSettings.startOnAxis = false;
    equityChartContents.chartCursorSettings.categoryBalloonEnabled = true;
    equityChartContents.chartCursorSettings.categoryBalloonText = "[[category]]";
    equityChartContents.chartCursorSettings.valueBalloonsEnabled = true;
    equityChartContents.chartCursorSettings.cursorColor = "rgb(200,0,0)";
    equityChartContents.chartScrollbarSettings.enabled = false;
    equityChartContents.dataDateFormat = "YYYY-MM-DDTJJ:NN:SS:QQQ";
    equityChartContents.panels.panel1.percentHeight = 60;
    equityChartContents.panels.panel1.showCategoryAxis = true;
    equityChartContents.panels.panel1.stockGraphs.graph1.fillAlphas=1;
    equityChartContents.panels.panel1.stockGraphs.graph1.fillColors= "rgb(175,230,238)";
    equityChartContents.panels.panel1.stockGraphs.graph1.id="graph1";
    equityChartContents.panels.panel1.stockGraphs.graph1.negativeBase=0;
    equityChartContents.panels.panel1.stockGraphs.graph1.negativeFillColors= "rgb(250,150, 150)";
    equityChartContents.panels.panel1.stockGraphs.graph1.type= "line";
    equityChartContents.panels.panel1.stockGraphs.graph1.useDataSetColors=false;
    equityChartContents.panels.panel1.stockGraphs.graph1.valueField= "equityData";
    equityChartContents.panels.panel2.percentHeight = 40;
    equityChartContents.panels.panel2.stockGraphs.graph2.fillAlphas=1;
    equityChartContents.panels.panel2.stockGraphs.graph2.id="graph2";
    equityChartContents.panels.panel2.stockGraphs.graph2.negativeBase = 0;
    equityChartContents.panels.panel2.stockGraphs.graph2.negativeFillColors= "rgb(250,150,150)";
    equityChartContents.panels.panel2.stockGraphs.graph2.type="line";
    equityChartContents.panels.panel2.stockGraphs.graph2.valueField="drawdownValue";
    equityChartContents.panels.panel2.stockGraphs.graph2.useDataSetColors = false;
    equityChartContents.panels.panel2.stockGraphs.graph3.id="graph3";
    equityChartContents.panels.panel2.stockGraphs.graph3.lineColor="rgb(250,0,0)";
    equityChartContents.panels.panel2.stockGraphs.graph3.type= "line";
    equityChartContents.panels.panel2.stockGraphs.graph3.useDataSetColors=false;
    equityChartContents.panels.panel2.stockGraphs.graph3.valueField= "drawdownValue";
    equityChartContents.valueAxesSettings.gridAlpha=.05;
    equityChartContents.valueAxesSettings.autoGridCount = true;*/
}


EquityAndDrawdown::~EquityAndDrawdown()
{
}

void EquityAndDrawdown::setChartTheme()
{
    EquityJsonFormat["theme"] = equityChartContents.theme;
}

void EquityAndDrawdown::setChartType()
{
    EquityJsonFormat["type"] = equityChartContents.type;
}

void EquityAndDrawdown::setDataDateFormat()
{
    EquityJsonFormat["dataDateFormat"] = equityChartContents.dataDateFormat;
}

void EquityAndDrawdown::setValueAxesSettings()
{
    EquityJsonFormat["valueAxesSettings"].insert("gridAlpha", this->equityChartContents.valueAxesSettings.gridAlpha);
    EquityJsonFormat["valueAxesSettings"].insert("autoGridCount", this->equityChartContents.valueAxesSettings.autoGridCount);
}

void EquityAndDrawdown::setChartScrollbarSettings()
{
    EquityJsonFormat["chartScrollbarSettings"].insert("enabled",this->equityChartContents.chartScrollbarSettings.enabled);
}

void EquityAndDrawdown::setChart_Panels()
{
    EquityJsonFormat["panels"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"][0].insert("showCategoryAxis",this->equityChartContents.panels.panel1.showCategoryAxis);
    EquityJsonFormat["panels"][0].insert("percentHeight",this->equityChartContents.panels.panel1.percentHeight);
    EquityJsonFormat["panels"][0].insert("stockGraphs", tsa::json::array{});
    EquityJsonFormat["panels"][0]["stockGraphs"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("id",this->equityChartContents.panels.panel1.stockGraphs.graph1.id);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("fillAlphas", this->equityChartContents.panels.panel1.stockGraphs.graph1.fillAlphas);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("negativeBase", this->equityChartContents.panels.panel1.stockGraphs.graph1.negativeBase);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("negativeFillColors", this->equityChartContents.panels.panel1.stockGraphs.graph1.negativeFillColors);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("fillColors", this->equityChartContents.panels.panel1.stockGraphs.graph1.fillColors);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("valueField", this->equityChartContents.panels.panel1.stockGraphs.graph1.valueField);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("type", this->equityChartContents.panels.panel1.stockGraphs.graph1.type);
    EquityJsonFormat["panels"][0]["stockGraphs"][0].insert("useDataSetColors", this->equityChartContents.panels.panel1.stockGraphs.graph1.useDataSetColors);
    //EquityJsonFormat["panels"][0]["stockGraphs"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"][1].insert("percentHeight", this->equityChartContents.panels.panel2.percentHeight);
    EquityJsonFormat["panels"][1].insert("stockGraphs", tsa::json::array{});
    EquityJsonFormat["panels"][1]["stockGraphs"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("id", this->equityChartContents.panels.panel2.stockGraphs.graph2.id);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("negativeBase", this->equityChartContents.panels.panel2.stockGraphs.graph2.negativeBase);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("negativeFillColors", this->equityChartContents.panels.panel2.stockGraphs.graph2.negativeFillColors);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("valueField",this->equityChartContents.panels.panel2.stockGraphs.graph2.valueField);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("type", this->equityChartContents.panels.panel2.stockGraphs.graph2.type);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("fillAlphas" , this->equityChartContents.panels.panel2.stockGraphs.graph2.fillAlphas);
    EquityJsonFormat["panels"][1]["stockGraphs"][0].insert("useDataSetColors", this->equityChartContents.panels.panel2.stockGraphs.graph2.useDataSetColors);
    EquityJsonFormat["panels"][1]["stockGraphs"].push_back(tsa::json::object{});
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("id", this->equityChartContents.panels.panel2.stockGraphs.graph3.id);
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("lineColor", this->equityChartContents.panels.panel2.stockGraphs.graph3.lineColor);
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("valueField", this->equityChartContents.panels.panel2.stockGraphs.graph3.valueField);
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("type", this->equityChartContents.panels.panel2.stockGraphs.graph3.type);
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("useDataSetColors", this->equityChartContents.panels.panel2.stockGraphs.graph3.useDataSetColors);
    EquityJsonFormat["panels"][1]["stockGraphs"][1].insert("balloon", tsa::json::object{});
    EquityJsonFormat["panels"][1]["stockGraphs"][1]["balloon"].insert("enabled", false);
}

void EquityAndDrawdown::setChart_DataSets(tsa::record_set&equityJsonData)
{
    EquityJsonFormat["dataSets"].push_back(tsa::json::object{});
    EquityJsonFormat["dataSets"][0].insert("categoryField", "equity_Date");
    EquityJsonFormat["dataSets"][0].insert("dataProvider",tsa::json::array{});
    EquityJsonFormat["dataSets"][0].insert("fieldMappings", tsa::json::array{});
    EquityJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    EquityJsonFormat["dataSets"][0]["fieldMappings"][0].insert("fromField", "equityData");
    EquityJsonFormat["dataSets"][0]["fieldMappings"][0].insert("toField","equityData");
    EquityJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
    EquityJsonFormat["dataSets"][0]["fieldMappings"][1].insert("fromField","drawdownValue");
    EquityJsonFormat["dataSets"][0]["fieldMappings"][1].insert("toField","drawdownValue");
    size_t nNumberOfRecords = equityJsonData.size();
    //equityJsonData[0][0].get<double>();
    //size_t col_equity = equity_data.column_position("cum_equity");
        auto dataProviderArray = EquityJsonFormat["dataSets"][0]["dataProvider"];
        for (size_t i = 0; i < nNumberOfRecords; i++) {
            //m_cEquityChart->add(equity_data.timestamp(i), (double)equity_data[i][0]);
            dataProviderArray.push_back(tsa::json::object{ { "equity_Date",equityJsonData.timestamp(i).to_string()},{ "equityData",equityJsonData[i][0].get<double>()},{ "drawdownValue",equityJsonData[i][1].get<double>() } });
        }


}

void EquityAndDrawdown::setCategoryAxesSettings()
{
    EquityJsonFormat["categoryAxesSettings"].insert("autoGridCount",this->equityChartContents.categoryAxesSettings.autoGridCount);
    EquityJsonFormat["categoryAxesSettings"].insert("equalSpacing",this->equityChartContents.categoryAxesSettings.equalSpacing);
    EquityJsonFormat["categoryAxesSettings"].insert("startOnAxis",this->equityChartContents.categoryAxesSettings.startOnAxis);
    EquityJsonFormat["categoryAxesSettings"].insert("minPeriod",this->equityChartContents.categoryAxesSettings.minPeriod);
    EquityJsonFormat["categoryAxesSettings"].insert("groupToPeriods", tsa::json::array{});
    EquityJsonFormat["categoryAxesSettings"]["groupToPeriods"].push_back("fff");
    EquityJsonFormat["categoryAxesSettings"].insert("dateFormats", tsa::json::array{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][0].insert("period", "fff");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][0].insert("format","JJ:NN:SS:QQQ");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][1].insert("period","ss");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][1].insert("format", "JJ:NN:SS:QQQ");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][2].insert("period","mm");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][2].insert("format", "JJ:NN:SS:QQQ");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][3].insert("period","hh");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][3].insert("format", "JJ:NN:SS:QQQ");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][4].insert("period","DD");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][4].insert("format", "YYYY-MM-DD");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][5].insert("period","MM");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][5].insert("format", "YYYY-MM-DD");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][6].insert("period","YYYY");
    EquityJsonFormat["categoryAxesSettings"]["dateFormats"][6].insert("format", "YYYY-MM-DD");
}

void EquityAndDrawdown::setChartCursorSettings()
{
    EquityJsonFormat["chartCursorSettings"].insert("valueBalloonsEnabled",this->equityChartContents.chartCursorSettings.valueBalloonsEnabled);
    EquityJsonFormat["chartCursorSettings"].insert("categoryBalloonEnabled" ,this->equityChartContents.chartCursorSettings.categoryBalloonEnabled);
    EquityJsonFormat["chartCursorSettings"].insert("categoryBalloonText",this->equityChartContents.chartCursorSettings.categoryBalloonText);
    EquityJsonFormat["chartCursorSettings"].insert("categoryBalloonDateFormats", tsa::json::array{});
    EquityJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"].push_back(tsa::json::object{});
    EquityJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("period", "fff");
    EquityJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("format","YYYY-MM-DD JJ:NN:SS:QQQ");
    EquityJsonFormat["chartCursorSettings"].insert("cursorColor", this->equityChartContents.chartCursorSettings.cursorColor);
}


void EquityAndDrawdown::setPanelsSettings()
{
    EquityJsonFormat["panelsSettings"].insert("usePrefixes",true);
}

void EquityAndDrawdown::setChartDiv(std::string chartDivName)
{
    this->equityChartContents.chartdiv = chartDivName;
}

std::string EquityAndDrawdown::getEquityJsonString(tsa::record_set&equityJsonData)
{
    size_t nNumberOfRecords = equityJsonData.size();
    if (nNumberOfRecords == 0) {
        std::string noDataFound = "SX_ERROR_EQUITY_CHART_NO_DATA";
        return noDataFound;
    }
    else
    {
        this->setChart_DataSets(equityJsonData);
        this->setCategoryAxesSettings();
        this->setChartCursorSettings();
        this->setChart_Panels();
        this->setPanelsSettings();
        this->setChartScrollbarSettings();
        this->setValueAxesSettings();
        std::string equityJsonString = this->EquityJsonFormat.to_string();
        std::string return_equity_jsonstring;
        return_equity_jsonstring.reserve(equityJsonString.size() + 10000);
        return_equity_jsonstring += " AmCharts.makeChart(\"";
        return_equity_jsonstring += equityChartContents.chartdiv + "\"," + equityJsonString + ");";
        //std::stringstream equityJsonStream;
        //equityJsonStream << "AmCharts.makeChart(\"" << equityChartContents.chartdiv << "\"," << equityJsonString << ");";

        //equityJsonString = equityJsonStream.str();

        return return_equity_jsonstring;
    }

}
