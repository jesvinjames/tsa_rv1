#pragma once
#include<iostream>
#include<sstream>
#include<fstream>

#include"../tsa/util_json.h"
#include"../tsa/db_record_set.h"

class CapitalAndPercentage
{
public:
	struct ChartContents {
		std::string chartdiv;
		std::string type;
		std::string theme;
		std::string	dataDateFormat;
		struct {
			bool autoGridCount;
			bool equalSpacing;
			bool startOnAxis;
			std::string minPeriod;
		}categoryAxesSettings;
		struct {
			bool autoGridCount;
			double gridAlpha;
		}valueAxesSettings;
		struct {
			bool enabled;
		}chartScrollbarSettings;
		struct {
			struct {
				bool showCategoryAxis;
				double percentHeight;
				struct {
					struct {
							std::string id;
							double fillAlphas;
							double negativeBase;
							std::string negativeFillColors;
							std::string	 fillColors;
							std::string	valueField;
							std::string	type;
							bool useDataSetColors;
					}graph1;
				}stockGraphs;

			}panel1;
			struct {
				double percentHeight;
				struct {
					std::string unit;
				}valueAxes;
				struct {
					struct {
						std::string id;
						double negativeBase;
						std::string negativeFillColors;
						std::string valueField;
						std::string type;
						double fillAlphas;
						bool useDataSetColors;
					}graph2;
					struct {
						std::string id;
						std::string lineColor;
						std::string valueField;
						std::string type;
						bool useDataSetColors;
					}graph3;

				}stockGraphs;
			}panel2;
		}panels;
		struct {
			bool valueBalloonsEnabled;
			bool categoryBalloonEnabled;
			std::string categoryBalloonText;
			std::string cursorColor;
		}chartCursorSettings;
	};
private:
	tsa::json capitalAndPercentageJsonFormat = tsa::json::object{};
	ChartContents capitalAndPercentageChartContents;
public:
	CapitalAndPercentage();
	~CapitalAndPercentage();
	void setChartTheme();
	void setChartType();
	void setDataDateFormat();
	void setValueAxesSettings();
	void setChartScrollbarSettings();
	void setChart_Panels();
	void setChart_DataSets(tsa::record_set &);
	void setCategoryAxesSettings();
	void setChartCursorSettings();
	void setPanelsSettings();
	void setChartDiv(std::string);
	std::string getCapitalAndPercentageJsonString(tsa::record_set &);
};

