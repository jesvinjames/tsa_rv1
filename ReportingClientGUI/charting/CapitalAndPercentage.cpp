#include "CapitalAndPercentage.h"



CapitalAndPercentage::CapitalAndPercentage()
{
	capitalAndPercentageJsonFormat.insert("type", "stock");
	capitalAndPercentageJsonFormat.insert("theme", "light");
	capitalAndPercentageJsonFormat.insert("categoryAxesSettings", tsa::json::object{});
	capitalAndPercentageJsonFormat.insert("valueAxesSettings", tsa::json::object{});
	capitalAndPercentageJsonFormat.insert("chartScrollbarSettings", tsa::json::object{});
	capitalAndPercentageJsonFormat.insert("dataSets", tsa::json::array{});
	capitalAndPercentageJsonFormat.insert("dataDateFormat", "YYYY-MM-DDTJJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat.insert("panels", tsa::json::array{});
	capitalAndPercentageJsonFormat.insert("chartCursorSettings", tsa::json::object{});
	capitalAndPercentageJsonFormat.insert("panelsSettings", tsa::json::object{});
	capitalAndPercentageChartContents.chartdiv = "chartdiv";
	capitalAndPercentageChartContents.type = "stock";
	capitalAndPercentageChartContents.theme = "light";
	capitalAndPercentageChartContents.categoryAxesSettings.autoGridCount = true;
	capitalAndPercentageChartContents.categoryAxesSettings.equalSpacing = true;
	capitalAndPercentageChartContents.categoryAxesSettings.minPeriod = "fff";
	capitalAndPercentageChartContents.categoryAxesSettings.startOnAxis = false;
	capitalAndPercentageChartContents.chartCursorSettings.categoryBalloonEnabled = true;
	capitalAndPercentageChartContents.chartCursorSettings.categoryBalloonText = "[[category]]";
	capitalAndPercentageChartContents.chartCursorSettings.valueBalloonsEnabled = true;
	capitalAndPercentageChartContents.chartCursorSettings.cursorColor = "rgb(200,0,0)";
	capitalAndPercentageChartContents.chartScrollbarSettings.enabled = false;
	capitalAndPercentageChartContents.dataDateFormat = "YYYY-MM-DDTJJ:NN:SS:QQQ";
	capitalAndPercentageChartContents.panels.panel1.percentHeight = 75;
	capitalAndPercentageChartContents.panels.panel1.showCategoryAxis = true;
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.fillAlphas = 1;
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.fillColors = "rgb(175,230,238)";
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.id = "graph1";
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.negativeBase = 0;
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.negativeFillColors = "rgb(250,150, 150)";
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.type = "line";
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.useDataSetColors = false;
	capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.valueField = "capitalData";
	capitalAndPercentageChartContents.panels.panel2.percentHeight = 25;
	capitalAndPercentageChartContents.panels.panel2.valueAxes.unit = "%";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.fillAlphas = 1;
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.id = "graph2";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.negativeBase = 0;
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.negativeFillColors = "rgb(250,150,150)";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.type = "line";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.valueField = "percentageValue";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.useDataSetColors = false;
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.id = "graph3";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.lineColor = "rgb(250,0,0)";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.type = "line";
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.useDataSetColors = false;
	capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.valueField = "percentageValue";
	capitalAndPercentageChartContents.valueAxesSettings.gridAlpha = .05;
	capitalAndPercentageChartContents.valueAxesSettings.autoGridCount = true;

}


CapitalAndPercentage::~CapitalAndPercentage()
{
}

void CapitalAndPercentage::setChartTheme()
{
	capitalAndPercentageJsonFormat["theme"] = this->capitalAndPercentageChartContents.theme;
}

void CapitalAndPercentage::setChartType()
{
	capitalAndPercentageJsonFormat["type"] = this->capitalAndPercentageChartContents.type;
}

void CapitalAndPercentage::setDataDateFormat()
{
	capitalAndPercentageJsonFormat["dataDateFormat"] = this->capitalAndPercentageChartContents.dataDateFormat;
}

void CapitalAndPercentage::setValueAxesSettings()
{
	capitalAndPercentageJsonFormat["valueAxesSettings"].insert("gridAlpha", this->capitalAndPercentageChartContents.valueAxesSettings.gridAlpha);
	capitalAndPercentageJsonFormat["valueAxesSettings"].insert("autoGridCount", this->capitalAndPercentageChartContents.valueAxesSettings.autoGridCount);

}

void CapitalAndPercentage::setChartScrollbarSettings()
{
	capitalAndPercentageJsonFormat["chartScrollbarSettings"].insert("enabled", this->capitalAndPercentageChartContents.chartScrollbarSettings.enabled);
}

void CapitalAndPercentage::setChart_Panels()
{
	capitalAndPercentageJsonFormat["panels"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][0].insert("showCategoryAxis", this->capitalAndPercentageChartContents.panels.panel1.showCategoryAxis);
	capitalAndPercentageJsonFormat["panels"][0].insert("percentHeight", this->capitalAndPercentageChartContents.panels.panel1.percentHeight);
	capitalAndPercentageJsonFormat["panels"][0].insert("stockGraphs", tsa::json::array{});
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("id", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.id);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("fillAlphas", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.fillAlphas);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("negativeBase", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.negativeBase);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("negativeFillColors", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.negativeFillColors);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("fillColors", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.fillColors);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("valueField", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.valueField);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("type", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.type);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"][0].insert("useDataSetColors", this->capitalAndPercentageChartContents.panels.panel1.stockGraphs.graph1.useDataSetColors);
	capitalAndPercentageJsonFormat["panels"][0]["stockGraphs"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][1].insert("percentHeight", this->capitalAndPercentageChartContents.panels.panel2.percentHeight);
	capitalAndPercentageJsonFormat["panels"][1].insert("valueAxes", tsa::json::array{});
	capitalAndPercentageJsonFormat["panels"][1]["valueAxes"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][1]["valueAxes"][0].insert("unit", this->capitalAndPercentageChartContents.panels.panel2.valueAxes.unit);
	capitalAndPercentageJsonFormat["panels"][1].insert("stockGraphs", tsa::json::array{});
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("id", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.id);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("negativeBase", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.negativeBase);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("negativeFillColors", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.negativeFillColors);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("valueField", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.valueField);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("type", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.type);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("fillAlphas", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.fillAlphas);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][0].insert("useDataSetColors", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph2.useDataSetColors);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("id", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.id);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("lineColor", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.lineColor);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("valueField", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.valueField);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("type", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.type);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("useDataSetColors", this->capitalAndPercentageChartContents.panels.panel2.stockGraphs.graph3.useDataSetColors);
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1].insert("balloon", tsa::json::object{});
	capitalAndPercentageJsonFormat["panels"][1]["stockGraphs"][1]["balloon"].insert("enabled", false);

}

void CapitalAndPercentage::setChart_DataSets(tsa::record_set &capitalJsonData)
{
	capitalAndPercentageJsonFormat["dataSets"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["dataSets"][0].insert("categoryField", "capital_Date");
	capitalAndPercentageJsonFormat["dataSets"][0].insert("dataProvider", tsa::json::array{});
	capitalAndPercentageJsonFormat["dataSets"][0].insert("fieldMappings", tsa::json::array{});
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"][0].insert("fromField", "capitalData");
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"][0].insert("toField", "capitalData");
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"][1].insert("fromField", "percentageValue");
	capitalAndPercentageJsonFormat["dataSets"][0]["fieldMappings"][1].insert("toField", "percentageValue");
	size_t nNumberOfRecords = capitalJsonData.size();
	//equityJsonData[0][0].get<double>();
	//size_t col_equity = equity_data.column_position("cum_equity");
	auto dataProviderArray = capitalAndPercentageJsonFormat["dataSets"][0]["dataProvider"];
	for (size_t i = 0; i < nNumberOfRecords; i++)
	{
		//m_cEquityChart->add(equity_data.timestamp(i), (double)equity_data[i][0]);
		dataProviderArray.push_back(tsa::json::object{ { "capital_Date",capitalJsonData.timestamp(i).to_string() },{ "capitalData",capitalJsonData[i][0].get<double>() },{ "percentageValue",capitalJsonData[i][1].get<double>() } });
	}
}

void CapitalAndPercentage::setCategoryAxesSettings()
{
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("autoGridCount", this->capitalAndPercentageChartContents.categoryAxesSettings.autoGridCount);
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("equalSpacing", this->capitalAndPercentageChartContents.categoryAxesSettings.equalSpacing);
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("startOnAxis", this->capitalAndPercentageChartContents.categoryAxesSettings.startOnAxis);
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("minPeriod", this->capitalAndPercentageChartContents.categoryAxesSettings.minPeriod);
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("groupToPeriods", tsa::json::array{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["groupToPeriods"].push_back("fff");
	capitalAndPercentageJsonFormat["categoryAxesSettings"].insert("dateFormats", tsa::json::array{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][0].insert("period", "fff");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][0].insert("format", "JJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][1].insert("period", "ss");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][1].insert("format", "JJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][2].insert("period", "mm");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][2].insert("format", "JJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][3].insert("period", "hh");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][3].insert("format", "JJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][4].insert("period", "DD");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][4].insert("format", "YYYY-MM-DD");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][5].insert("period", "MM");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][5].insert("format", "YYYY-MM-DD");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][6].insert("period", "YYYY");
	capitalAndPercentageJsonFormat["categoryAxesSettings"]["dateFormats"][6].insert("format", "YYYY-MM-DD");

}

void CapitalAndPercentage::setChartCursorSettings()
{
	capitalAndPercentageJsonFormat["chartCursorSettings"].insert("valueBalloonsEnabled", this->capitalAndPercentageChartContents.chartCursorSettings.valueBalloonsEnabled);
	capitalAndPercentageJsonFormat["chartCursorSettings"].insert("categoryBalloonEnabled", this->capitalAndPercentageChartContents.chartCursorSettings.categoryBalloonEnabled);
	capitalAndPercentageJsonFormat["chartCursorSettings"].insert("categoryBalloonText", this->capitalAndPercentageChartContents.chartCursorSettings.categoryBalloonText);
	capitalAndPercentageJsonFormat["chartCursorSettings"].insert("categoryBalloonDateFormats", tsa::json::array{});
	capitalAndPercentageJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"].push_back(tsa::json::object{});
	capitalAndPercentageJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("period", "fff");
	capitalAndPercentageJsonFormat["chartCursorSettings"]["categoryBalloonDateFormats"][0].insert("format", "YYYY-MM-DD JJ:NN:SS:QQQ");
	capitalAndPercentageJsonFormat["chartCursorSettings"].insert("cursorColor", this->capitalAndPercentageChartContents.chartCursorSettings.cursorColor);
}


void CapitalAndPercentage::setPanelsSettings()
{
	capitalAndPercentageJsonFormat["panelsSettings"].insert("usePrefixes", true);
}

void CapitalAndPercentage::setChartDiv(std::string chartDiv)
{
	this->capitalAndPercentageChartContents.chartdiv =chartDiv;
}

std::string CapitalAndPercentage::getCapitalAndPercentageJsonString(tsa::record_set &capitalJsonData)
{
	size_t nNumberOfRecords = capitalJsonData.size();
	if (nNumberOfRecords == 0) {
		std::string noDataFound = "SX_ERROR_EQUITY_CHART_NO_DATA";
		return noDataFound;
	}
	else
	{
		this->setChart_DataSets(capitalJsonData);
		this->setCategoryAxesSettings();
		this->setChartCursorSettings();
		this->setChart_Panels();
		this->setPanelsSettings();
		this->setChartScrollbarSettings();
		this->setValueAxesSettings();
		std::string equityJsonString = this->capitalAndPercentageJsonFormat.to_string();
		std::stringstream equityJsonStream;
        equityJsonStream << "AmCharts.makeChart(\"" << this->capitalAndPercentageChartContents.chartdiv << "\"," << equityJsonString << ");";
		equityJsonString = equityJsonStream.str();
		return equityJsonString;
	}
}
