#pragma once
#include<iostream>
#include<sstream>
#include<fstream>
#include"../tsa/util_json.h"
#include"../tsa/db_record_set.h"
#include"../tsa/sys_metrics.h"
class EquityAndDrawdown
{
public:
    struct ChartContents {
                            std::string chartdiv= "chartdiv";
                            std::string type= "stock";
                            std::string theme= "light";
                            std::string	dataDateFormat= "YYYY-MM-DDTJJ:NN:SS:QQQ";
                            struct {
                                bool autoGridCount= true;
                                bool equalSpacing= true;
                                bool startOnAxis= false;
                                std::string minPeriod= "fff";
                            }categoryAxesSettings;
                            struct {
                                bool autoGridCount= true;
                                double gridAlpha= .05;
                            }valueAxesSettings;
                            struct {
                                bool enabled= false;
                            }chartScrollbarSettings;
                            struct {
                                struct{
                                bool showCategoryAxis= true;
                                double percentHeight= 60;
                                struct {
                                    struct {
                                        std::string id= "graph1";
                                        double fillAlphas=1;
                                        double negativeBase=0;
                                        std::string negativeFillColors= "rgb(250,150, 150)";
                                        std::string	 fillColors= "rgb(175,230,238)";
                                        std::string	valueField= "equityData";
                                        std::string	type= "line";
                                        bool useDataSetColors= false;
                                    }graph1;
                                }stockGraphs;

                                }panel1;
                                struct {
                                    double percentHeight= 40;
                                        struct {
                                            struct {
                                                std::string id= "graph2";
                                                double negativeBase=0;
                                                std::string negativeFillColors= "rgb(250,150,150)";
                                                std::string valueField= "drawdownValue";
                                                std::string type= "line";
                                                double fillAlphas=1;
                                                bool useDataSetColors= false;
                                            }graph2;
                                            struct {
                                                std::string id= "graph3";
                                                std::string lineColor= "rgb(250,0,0)";
                                                std::string valueField= "drawdownValue";
                                                std::string type= "line";
                                                bool useDataSetColors= false;
                                            }graph3;

                                }stockGraphs;
                                }panel2;
                            }panels;
                            struct {
                                bool valueBalloonsEnabled= true;
                                bool categoryBalloonEnabled= true;
                                std::string categoryBalloonText= "[[category]]";
                                std::string cursorColor= "rgb(200,0,0)";
                            }chartCursorSettings;
    };
private:
    tsa::json EquityJsonFormat = tsa::json::object{};
    ChartContents equityChartContents;
public:
    EquityAndDrawdown();
    ~EquityAndDrawdown();
    void setChartTheme();
    void setChartType();
    void setDataDateFormat();
    void setValueAxesSettings();
    void setChartScrollbarSettings();
    void setChart_Panels();
    void setChart_DataSets(tsa::record_set &);
    void setCategoryAxesSettings();
    void setChartCursorSettings();
    void setPanelsSettings();
    void setChartDiv(std::string);
    std::string getEquityJsonString(tsa::record_set &);
};

