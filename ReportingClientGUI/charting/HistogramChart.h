#pragma once
#include"HistogramJsonData.h"
#include"HistogramJson.h"

class HistogramChart
{
public:
	HistogramChart();
	~HistogramChart();
	std::string getHistogramString(std::vector<double>&, std::string strDivName_i);
};

