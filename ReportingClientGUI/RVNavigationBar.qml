import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1


Rectangle{
 color: "#EFEFEF"
 Layout.minimumHeight: 30
 Layout.maximumHeight: 30
 Layout.maximumWidth: main_layout.width-1
border.color: "#EEEEEE"
border.width: 0
Layout.rightMargin: 4
Layout.leftMargin: 2

function clear_combo(){
    cbItems.clear()
}

function get_selected_instrument(){
    return instrument_combo.currentIndex
}

function select_first_instrument(){
    instrument_combo.currentIndex = 0
}

function add_instrument(instrument_name){
    cbItems.append({key:instrument_name})
}

function setStrategy(strategyName){
    strategy_display.text = strategyName
}


 RowLayout{
     Layout.fillHeight: true
     spacing: 0
/*
 Rectangle{
     //anchors.left: navigation_bar.left
     color:"#202024"
     Layout.minimumHeight: 30
     Layout.maximumHeight: 30
     Layout.maximumWidth: 20
     Layout.minimumWidth: 20

 }*/

     Component.onCompleted: {
        // add_instrument("instrument 1")


    }


 Button{
     id:strategy_display
     Layout.minimumHeight: 30
     Layout.maximumHeight: 30

     text: "Strategy"
     font.family: "Arial"; font.pointSize: 10;font.bold: true



     background: Rectangle {

         id:back
         opacity: enabled ? 1 : 0.3

         color:"white"
     }

 }
 /*Button{
     Layout.minimumHeight: 30
     Layout.maximumHeight: 30
     font.family: "Arial"; font.pointSize: 10;font.bold: true

     text: ">"

     background: Rectangle {
         opacity: enabled ? 1 : 0.3

         color:"white"
     }
 }*/

/* Button{
     Layout.minimumHeight: 30
     Layout.maximumHeight: 30
     font.family: "Arial"; font.pointSize: 10;font.bold: true

     text: "Instrument 1"

     background: Rectangle {

         opacity: enabled ? 1 : 0.3

         color:"white"
     }
 }*/



 ComboBox {
     id: instrument_combo
     font.family: "Arial"; font.pointSize: 10;font.bold: true

     Layout.minimumHeight: 30
     Layout.maximumHeight: 30

     textRole: "key"
         model: ListModel {
             id:cbItems

     }

     onCurrentIndexChanged:
     {
         console.debug(currentIndex)
         //cbItems.append({key: "First", value: 123})
         strategy_manager.open_instrument(currentIndex)
         rv_toolbar.clickButton(1)
         menu_strategies.clickButton(0)
     }

 }

 Text{
     color:"white"

     Layout.fillHeight: true

     font.family: "Arial"; font.pointSize: 18;


 }

 /*TextField {
     id:t1
     placeholderText: "Strategy001 > Instrument1 > Performance"
     Layout.fillHeight: true
     Layout.fillWidth: true
     font.family: "Arial"; font.pointSize: 9;
     opacity: enabled ? 1.0 : 0.3
     //color: b1.down ? "white" : "black"
    // horizontalAlignment: Text.AlignHCenter
     //verticalAlignment: Text.AlignVCenter

     color: "black"
     background: Rectangle {
         color: "#EFEFEF"
         border.color: "#666666"
         border.width: 1
     }



 }*/
 }


}
