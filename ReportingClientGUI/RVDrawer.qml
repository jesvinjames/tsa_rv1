import QtQuick 2.0
import QtQuick.Controls 2.1

Drawer {

        id: settings_drawer
        property bool is_opened: false
        edge: Qt.RightEdge
        y: rv_toolbar.height
        width: 500
        height: window.height - rv_toolbar.height-4

        onClosed:
        {
            is_opened = false
        }
        onOpened: {
            is_opened = true

        }

        Rectangle{
            Rectangle{
                implicitHeight: 60
                implicitWidth: 500
                color:"#454545"
                Text{
                    text:"Settings"
                    anchors.centerIn: parent
                    color: "white"
                    font.family: "Arial"; font.pointSize: 13;font.bold: true

                }
            }

            anchors.fill: parent
            color:"#EFEFEF"

            border.width: 0

            Text {
                id: text1
                anchors.centerIn: parent
                text: qsTr("This is a drawer. This place can be used for pages like settings")
            }
        }
    }
