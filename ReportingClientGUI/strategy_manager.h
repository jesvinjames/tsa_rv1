#ifndef METRICS_MANAGER_H
#define METRICS_MANAGER_H

#include "sys_metrics.h"
#include <string>
#include <QObject>
#include "html_template.h"
#include "chart_manager.h"

#include "charting/HistogramChart.h"
#include "charting/EquityAndDrawdown.h"
#include "charting/CapitalAndPercentage.h"
#include <map>

namespace tsarv {
class strategy_manager: public QObject
{
public:
    Q_OBJECT
public:
    enum metrics{strategy,instrument};
    tsarv::chart_manager* m_chart_manager;

public:
    void init();
public:
    explicit strategy_manager(QObject *parent = 0);
    Q_INVOKABLE void open_strategy(QString strategy_path);
    Q_INVOKABLE void open_instrument(int instrument_no);
    Q_INVOKABLE void init_templates();


    Q_INVOKABLE QString get_equity_chart_string();
    Q_INVOKABLE QString get_histogram_chart_string();
    Q_INVOKABLE QString get_drawdown_chart_string();
    Q_INVOKABLE QString get_capital_percentage_chart_string();

    Q_INVOKABLE QString get_strategy_name();


    tsa::metrics* get_metrics(metrics type);

    Q_INVOKABLE QString get_template_string(QString template_name);
    Q_INVOKABLE int get_instrument_count();
protected:
    std::string m_strategy_path;
    tsa::metrics* m_strategy_metrics;
    tsa::metrics* m_instrument_metrics;
    int m_instrument_no;
    int m_num_of_instr;
    bool is_instrument;

    EquityAndDrawdown m_equity_drawdown_chart;
    HistogramChart m_histogram_chart;
    CapitalAndPercentage m_capital_percentage_chart;

    std::map<std::string,html_template*> m_templates;

};
}
#endif // METRICS_MANAGER_H
