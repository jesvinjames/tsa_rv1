#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QCoreApplication>
#include <QtWebEngineCore/QtWebEngineCore>
#include <QtWebEngine/QtWebEngine>
#include <QtWebView>

#include "person.h"

#include "html_template.h"
#include "qml_html_template.h"
#include "chart_manager.h"
#include "strategy_manager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QtWebView::initialize();



   int id = qmlRegisterType<Person>("org.sdc.people", 1,0, "Person");

    qmlRegisterType<tsarv::qml::html_template>("org.sdc.tsarv.template", 1,0, "HTMLTemplate");
    qmlRegisterType<tsarv::chart_manager>("org.sdc.tsarv.chartmanager", 1,0, "ChartManager");
    qmlRegisterType<tsarv::strategy_manager>("org.sdc.tsarv.strategymanager", 1,0, "StrategyManager");



    qDebug()<<id;
    std::cout<<"hi1";

    QQmlApplicationEngine engine;
   // qmlRegisterType<Test>("org.sdc.Qt.QQmlExtensionInterface", 1, 0, "Test");

    std::string tbody = "name:${name} and age:${age} ";
    //tsarv::html_template ptemp;
    //ptemp.set_template("resh/templates/template_performance.html");

    //ptemp.load_keys();

    engine.rootContext()->setContextProperty("applicationPath", "file://"+qApp->applicationDirPath()+ "/");

    engine.load(QUrl(QLatin1String("qrc:/RV_GUI.qml")));

    return app.exec();
/*
    std::regex std_rx (R"del(XYZ\d\d)del");
       std::string str = " XYZ111 d-dxxxxxxx XYZ222 t-nyyyyyyyyy XYZ333 t-r ";

       std::cout << "std::regex way: " << std::regex_replace (str, std_rx, "A") << std::endl
                 << "pcrecpp way: ";
*/

}
