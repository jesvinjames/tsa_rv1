import QtQuick 2.0
import QtQuick 2.5
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4 as Controls14

import QtQuick.Layouts 1.3
import QtQml.Models 2.2

import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

import QtQuick.Controls 2.1
import QtQuick.Controls 1.1

import QtQuick.Extras 1.4

import QtQuick 2.7


import QtWebEngine 1.3

Rectangle{

    property var container: null
    property var rc_gui:null
    property var controller:null

    implicitHeight: container.height
    implicitWidth: container.width
    color: "white"

    ColumnLayout {

        id: main_layout
        Layout.fillWidth: true
        spacing: 0

        Rectangle{
            Layout.topMargin: 0
            Layout.leftMargin: 0
            Layout.bottomMargin: 0
            Layout.minimumHeight: 25
            Layout.minimumWidth: container.width
            color:"gray"

        Text {
            anchors.centerIn: parent
            Layout.leftMargin: 0
            text: qsTr("Input Single Strategy Path")
            font.family: "Arial"; font.pointSize: 11;
            color: "white"



        }
        }

        TextField{
            Layout.minimumWidth: container.width
            height: 40
            id:path_text
            text: "E:\\Work\\Lib\\TS_API\\tutorial\\output\\305_trading"
        }

        Button{
            id: load
            text: "Load"
            anchors.horizontalCenter:  main_layout.horizontalCenter
            Layout.topMargin: 2
            onClicked: {
                rc_gui.loadTemplates(path_text.text)
                rv_toolbar.clickButton(1)
                menu_strategies.clickButton(0)
                rv_toolbar.enableChartOption()
               // navigationBar.clickButton(1)


            }

        }



        /*ComboBox {
            Layout.topMargin: 0
            Layout.leftMargin: 0
            Layout.bottomMargin: 0

            editable:true

            Layout.minimumWidth: container.width
            Layout.maximumWidth: container.width
            model: ListModel {
                    id: model
                    ListElement { text: "Banana"; color: "Yellow" }
                    ListElement { text: "Apple"; color: "Green" }
                    ListElement { text: "Coconut"; color: "Brown" }
                }

            function add(item){
                model.append({text: item})
                currentIndex = find(item)
            }

             onCurrentIndexChanged:{
                 add("wow")
             }
        }*/

       /* Rectangle{
            Layout.topMargin: 0
            Layout.leftMargin: 0
            Layout.bottomMargin: 0
            Layout.minimumHeight: 25
            Layout.minimumWidth: container.width
            color:"gray"

        Text {
            anchors.centerIn: parent
            Layout.leftMargin: 0
            text: qsTr("Strategies")
            font.family: "Arial"; font.pointSize: 11;
            color: "white"



        }
        }*/
    }


}













