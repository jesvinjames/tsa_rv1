QT += core qml quick webview webengine webenginewidgets

SOURCES += main.cpp \
    person.cpp \
    html_template.cpp \
    qml_html_template.cpp \
    am_charts.cpp \
    strategy_manager.cpp \
    json_chart.cpp \
    chart_manager.cpp \
    charting/CapitalAndPercentage.cpp \
    charting/EquityAndDrawdown.cpp \
    charting/HistogramChart.cpp \
    charting/HistogramJson.cpp \
    charting/HistogramJsonData.cpp

RESOURCES += qml.qrc



# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    person.h \
    html_template.h \
    qml_html_template.h \
    am_charts.h \
    strategy_manager.h \
    json_chart.h \
    chart_manager.h \
    charting/CapitalAndPercentage.h \
    charting/EquityAndDrawdown.h \
    charting/HistogramChart.h \
    charting/HistogramJson.h \
    charting/HistogramJsonData.h


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../Lib/TS_API/lib/VS2015/release/ -lTS_API_LIB
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../Lib/TS_API/lib/VS2015/debug/ -lTS_API_LIB
else:unix: LIBS += -L$$PWD/../../Lib/TS_API/lib/VS2015/ -lTS_API_LIB

INCLUDEPATH += $$PWD/../../Lib/TS_API/inc/tsa
DEPENDPATH += $$PWD/../../Lib/TS_API/inc/tsa

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/release/libTS_API_LIB.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/debug/libTS_API_LIB.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/release/TS_API_LIB.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/debug/TS_API_LIB.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/libTS_API_LIB.a

DISTFILES +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../Lib/TS_API/lib/VS2015/release/ -lTS_CHART_LIB
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../Lib/TS_API/lib/VS2015/debug/ -lTS_CHART_LIB

INCLUDEPATH += $$PWD/../../Lib/TS_API/_internal/tsc/inc
DEPENDPATH += $$PWD/../../Lib/TS_API/_internal/tsc/inc

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/release/libTS_CHART_LIB.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/debug/libTS_CHART_LIB.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/release/TS_CHART_LIB.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../Lib/TS_API/lib/VS2015/debug/TS_CHART_LIB.lib



