#include "strategy_manager.h"

namespace tsarv {

    strategy_manager::strategy_manager(QObject *parent): QObject(parent)
    {
        m_num_of_instr = 1;
        is_instrument= false;
        m_strategy_path = "";
        init();
    }

    void strategy_manager::open_strategy(QString strategy_path)
    {
        try
        {
            m_strategy_path = strategy_path.toStdString();
            m_strategy_metrics = new tsa::metrics();
            m_strategy_metrics->read(m_strategy_path + "//strategy_metrics.bin");
            m_num_of_instr = m_strategy_metrics->num_instruments();
            //m_chart_manager->set_instrument_number(0);
            //m_chart_manager->set_strategy_path(strategy_path);
           // m_chart_manager->set_metrices(m_strategy_metrics,m_instrument_metrics);
            is_instrument= false;

            m_templates["info-strategy"]->unload();
            m_templates["info-instrument"]->unload();
            m_templates["info-strategy-instrument"]->unload();
            m_templates["performance"]->unload();
            m_templates["risk"]->unload();
            m_templates["returns"]->unload();

        }
        catch(std::exception e)
        {
            qDebug(e.what());
        }

    }

    QString strategy_manager::get_strategy_name()
    {
        return m_strategy_metrics->strategy_name().c_str();
    }


    QString strategy_manager::get_equity_chart_string()
    {
        EquityAndDrawdown m_equity_drawdown_chart;
        m_equity_drawdown_chart.setChartDiv("equity_chart");

        std::string script = m_equity_drawdown_chart.getEquityJsonString(m_strategy_metrics->daily_equity_series());

        //std::cout<<"\n\n\n\nEquity\n\n\n\n"<<script<<"\n\n\n"<<std::flush;
        return script.c_str();
    }

    QString strategy_manager::get_histogram_chart_string()
    {
        std::string hist = "";
        HistogramChart m_histogram_chart;
        std::vector<double> m_vdHistogramData = m_strategy_metrics->trade_net_profits();
        if (m_vdHistogramData.size() != 0)
        {
            hist = m_histogram_chart.getHistogramString(m_vdHistogramData, "histogram_chart");
        }

        return hist.c_str();

    }

    QString strategy_manager::get_drawdown_chart_string()
    {
        return "";

    }

    QString strategy_manager::get_capital_percentage_chart_string()
    {
        CapitalAndPercentage m_capital_percentage_chart;
        m_capital_percentage_chart.setChartDiv("chartdiv");

        std::string strAMChartsRiskString;

        tsa::record_set record_set_risk = m_strategy_metrics->report_capital_series(10000);

        if (record_set_risk.size() != 0)
        {
            strAMChartsRiskString = m_capital_percentage_chart.getCapitalAndPercentageJsonString(record_set_risk);
        }
        return strAMChartsRiskString.c_str();

    }

    void strategy_manager::open_instrument(int instrument_no)
    {
        if( instrument_no == 0)
        {
            is_instrument = false;
            m_templates["info-strategy"]->unload();
            m_templates["info-instrument"]->unload();
            m_templates["info-strategy-instrument"]->unload();
            m_templates["performance"]->unload();
            m_templates["risk"]->unload();
            m_templates["returns"]->unload();
        }
        else
        {
        try
        {

            m_instrument_metrics = new tsa::metrics();
            m_instrument_metrics->read(m_strategy_path + "//instruments//instr_metrics_" + std::to_string(instrument_no-1) + ".bin");
            //m_chart_manager->set_instrument_number(0);
            //m_chart_manager->set_strategy_path(strategy_path);
            is_instrument = true;
            //m_chart_manager->set_metrices(m_strategy_metrics,m_instrument_metrics);
            std::cout<<"\ninstrument " <<instrument_no-1<<" loaded\n";

            m_templates["info-strategy"]->unload();
            m_templates["info-instrument"]->unload();
            m_templates["info-strategy-instrument"]->unload();
            m_templates["performance"]->unload();
            m_templates["risk"]->unload();
            m_templates["returns"]->unload();

        }
        catch(std::exception e)
        {
            qDebug(e.what());
        }
        }
    }

    int strategy_manager::get_instrument_count()
    {
        return m_num_of_instr;
    }

    tsa::metrics* strategy_manager::get_metrics(metrics type)
    {
        if( type == strategy_manager::metrics::strategy)
        {
            return m_strategy_metrics;
        }
        else if(type == strategy_manager::metrics::instrument)
        {
            return m_instrument_metrics;
        }
        else
        {
            return m_strategy_metrics;
        }
    }

    void strategy_manager::init()
    {
        //m_chart_manager = new tsarv::chart_manager();
    }

    QString strategy_manager::get_template_string(QString template_name)
    {
       // Check if element exists in map or not
       if (m_templates.count(template_name.toStdString()) > 0)
       {
           if( is_instrument )
           {
               std::cout<<"\n\n\n\nInstrument\n\n\n";
               if( !m_templates[template_name.toStdString()]->is_loaded())
               {
                   m_templates[template_name.toStdString()]->clear();
                   m_templates[template_name.toStdString()]->reset_keys();

                   if(template_name == "performance")
                   {
                       std::cout<<"\n\nyes\n\n"<<std::flush;
                       m_templates[template_name.toStdString()]->set_key("${equity_chart_script|script}",get_equity_chart_string().toStdString());
                       m_templates[template_name.toStdString()]->set_key("${histogram_chart_script|script}",get_histogram_chart_string().toStdString());

                   }
                   if(template_name == "risk")
                   {
                       m_templates[template_name.toStdString()]->set_key("${capital_chart_script|script}",get_capital_percentage_chart_string().toStdString());

                   }

                   m_templates[template_name.toStdString()]->processTemplate(m_instrument_metrics,m_instrument_metrics);
                   m_templates[template_name.toStdString()]->process();



               }
               return QString(m_templates[template_name.toStdString()]->template_string().c_str());
           }
           else
           {
               std::cout<<"\n\n\n\nStrategy\n\n\n";

               if( !m_templates[template_name.toStdString()]->is_loaded())
               {
                   m_templates[template_name.toStdString()]->clear();
                   m_templates[template_name.toStdString()]->reset_keys();

                   if(template_name == "performance")
                   {
                       m_templates[template_name.toStdString()]->set_key("${equity_chart_script|script}",get_equity_chart_string().toStdString());
                       m_templates[template_name.toStdString()]->set_key("${histogram_chart_script|script}",get_histogram_chart_string().toStdString());

                   }
                   if(template_name == "risk")
                   {
                       m_templates[template_name.toStdString()]->set_key("${capital_chart_script|script}",get_capital_percentage_chart_string().toStdString());

                   }

                   m_templates[template_name.toStdString()]->processTemplate(m_strategy_metrics,m_strategy_metrics);
                   m_templates[template_name.toStdString()]->process();


               }
               return QString(m_templates[template_name.toStdString()]->template_string().c_str());
           }

       }

       return "Error";

    }

    void strategy_manager::init_templates()
    {
        m_templates["info-strategy"] = new html_template("res/templates/template_strategy_info.html");
        m_templates["info-instrument"] = new html_template("res/templates/template_instrument_info.html");
        m_templates["info-strategy-instrument"] = new html_template("res/templates/template_strat_intr_info.html");

        m_templates["performance"] = new html_template("res/templates/template_performance.html");

        //std::cout<<"\n\n\n\nInitial\n\n\n\n"<< m_templates["performance"]->template_string()<<"\n\n\n\n"<<std::flush;

        m_templates["risk"] = new html_template("res/templates/template_risk.html");
        m_templates["returns"] = new html_template("res/templates/template_returns.html");


    }
}
