import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Item {
    property alias button1: button1
    property alias button2: button2

    Button {
        id: button1
        x: 217
        y: 220
        text: qsTr("Press Me 1")
    }

    Button {
        id: button2
        x: 322
        y: 220
        text: qsTr("Press Me 2")
    }

     Dial {
         id: dial1
         x: 271
         y: 72
         width: 87
         height: 90
     }

     ProgressBar {
         id: progressBar1
         x: 137
         y: 189
         value: 0.5
     }

     RadioButton {
         id: radioButton1
         x: 92
         y: 83
         text: qsTr("Radio Button")
     }

    RadioDelegate {
        id: radioDelegate1
        x: 427
        y: 72
        text: qsTr('Radio Delegate')
    }

    BusyIndicator {
        id: busyIndicator1
        x: 69
        y: 235
    }

    CheckBox {
        id: checkBox1
        x: 69
        y: 315
        text: qsTr("Check Box")
    }

    ComboBox {
        id: comboBox1
        x: 197
        y: 292
    }

    Label {
        id: label1
        x: 403
        y: 166
        text: qsTr("Label")
    }

    Slider {
        id: slider1
        x: 176
        y: 26
        value: 0.5
    }

    SpinBox {
        id: spinBox1
        x: 218
        y: 379
    }

    Switch {
        id: switch1
        x: 371
        y: 305
        text: qsTr("Switch")
    }

    TextArea {
        id: textArea1
        x: 494
        y: 195
        width: 88
        height: 87
        text: qsTr("Text Area")
    }

    TextField {
        id: textField1
        x: 412
        y: 26
        text: qsTr("Text Field")
    }

    ToolBar {
        id: toolBar1
        x: 135
        y: 425
        width: 360
    }
}
