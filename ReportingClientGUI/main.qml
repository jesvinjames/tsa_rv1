import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0


import QtWebEngine 1.3


ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")



    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            RowLayout {
                id: layout
                anchors.fill: parent
                spacing: 1

                Rectangle {
                    anchors.fill: parent

                        color: 'teal'

                        Text {
                            text: parent.width + 'x' + parent.height
                        }
                    }

            TabView {
               id: tabwidget
               anchors.fill: parent

               Tab {
                      title: "Charts"
                      Rectangle { color: "red"
                          anchors.fill: parent

                      WebEngineView
                      {
                          anchors.fill: parent

                          url:"file:///C:/Users/ansalpa/Downloads/startbootstrap-sb-admin-2-gh-pages/startbootstrap-sb-admin-2-gh-pages/index.html"
                      }
}

                  }
                  Tab {
                      title: "Summary"
                      Rectangle { color: "blue" }
                  }
                  Tab {
                      title: "Trades"
                      Rectangle { color: "green" }
                  }
                  Tab {
                      title: "Risk"
                      Rectangle { color: "green" }
                  }
                  Tab {
                      title: "Units"
                      Rectangle { color: "green" }
                  }

               }
            }

        }

        Page {
            Label {
                text: qsTr("Second page")
                anchors.centerIn: parent
            }
        }
        Page {


            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                spacing: 5

            Button{
                id:button1
                Layout.alignment: Qt.AlignCenter
                text:"Open"
            }
            Button{
                id:button2
                Layout.alignment: Qt.AlignCenter
                text:"Close"
            }
            }
        }
    }

    header: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Strategy")
        }
        TabButton {
            text: qsTr("Instruments")
        }
        TabButton {
            text: qsTr("File")

        }
    }
}
