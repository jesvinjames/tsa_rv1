import QtQuick 2.0

Item {

    property RVToggleButton current_button:null
    property int button_count:0
    property var buttons_in_group

    function add_button(button) {
        if( button.object_type === "rv_toggle_button")
        {
            buttons_in_group[button_count] = button
            button.button_group=id
            button_count++
        }
    }

    function set_pressed_button(button)
    {
        for(var i=0;i<button_count;i++)
        {
            buttons_in_group[button_count].button_toggled = false;
        }

       button.button_toggled = true;

    }
}
