#include "qml_html_template.h"
#include "sys_metrics.h"


namespace tsarv {
namespace qml {

    html_template::html_template(QObject *parent) : QObject(parent),tsarv::html_template()
    {

       // process();
    }

    bool html_template::set_template(QString template_file)
    {
        tsarv::html_template::set_template(template_file.toStdString());
        load_keys();
        return true;
    }
    QString html_template::get_strategy_name()
    {
         QString strategy_name = "Demo";
        try
        {
            strategy_name =  metrics_obj->strategy_name().c_str();
        }
        catch(...){

        }

         return strategy_name;
    }

    void html_template::load_metrics(QString strategy_path)
    {

        try
        {
        tsa::metrics* cptrMetrics = new tsa::metrics();
        metrics_obj = cptrMetrics;

        cptrMetrics->read(strategy_path.toStdString() + "//strategy_metrics.bin");
        processTemplate(cptrMetrics,cptrMetrics);
        }
        catch(std::exception e)
        {
            qDebug(e.what());
        }

        process();


    }

    void html_template::load_metrics(tsa::metrics* metrics)
    {
        try
        {
        metrics_obj = metrics;

        processTemplate(metrics,metrics);
        }
        catch(std::exception e)
        {
            qDebug(e.what());
        }

        process();

    }

    QString html_template::get_template_string()
    {
        QString qtemplate_body(template_body.c_str());
        return qtemplate_body;
    }


}
}
