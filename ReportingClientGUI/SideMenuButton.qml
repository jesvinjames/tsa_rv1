import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3


Button {
      id: b1

      property bool button_toggled: false
      property string object_type :"rv_toggle_button"
      property var parent_toolbar : null
      property string color_normal:"#343434"
      property string color_pressed:"#787878"
      property string color_hover_normal:"#505050"
      property string color_hover_pressed:"#888888"

      property string color_text_normal:"white"
      property string color_text_pressed:"white"


      property string bg_color:"#000511"
      property string bg_color_pressed:"#000511"


      function setBackgroundColor(color)
      {
          color_normal = color
          return color
      }



      function toggle(turn_on)
      {
          button_toggled = turn_on
          back.color = button_toggled?color_pressed:color_normal
          button_text.color=button_toggled?color_text_pressed:color_text_normal
          //clicked_sign.visible = button_toggled?true:false
          clicked_sign.color =  button_toggled? "#FF9F00":color_normal



      }

    font.family: "Areal"; font.pointSize: 10;

        width: 300
          text:"File"


          contentItem:
              Rectangle{
              implicitHeight: 30
              color: "transparent"
            RowLayout{
                spacing: 0
                  Rectangle{
                      implicitWidth: 50
                      implicitHeight: 30
                      color:"transparent"
                  }
              Text {
                    id:button_text
                    text: b1.text
                    opacity: enabled ? 1.0 : 0.3
                    font.family: "Areal"; font.pointSize: 10;

                    //color: b1.down ? "white" : "black"
                    color:color_text_normal
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignHCenter
                    elide: Text.ElideRight
                    anchors.leftMargin: 20
                }}
}

                background: Rectangle {


                    RowLayout{


                    Rectangle{
                        id:clicked_sign
                        implicitWidth: 5
                        implicitHeight: 42
                        color: back.color
                        visible: true
                    }


                    Image {

                        id:settings_icon

                        source: "other.png"


                    Layout.maximumHeight:  32
                    Layout.maximumWidth: 32
                    }
                    Rectangle{
                    implicitWidth: 100
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    //border.color: button_toggled ? "#FFA500" :color_normal
                    border.color: "transparent"
                    border.width: 1

                    radius: 0

                    color: back.color
                    }
                }
                    id:back
                    implicitWidth: 100
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    //border.color: button_toggled ? "#FFA500" :color_normal
                    border.color: "transparent"
                    border.width: 1

                    radius: 0

                    color:color_normal



          MouseArea {
              hoverEnabled: true
              anchors.fill: parent;
              onPressed: {



                  //back.color = button_toggled?"white":"gray"



              }
              onClicked: {

                  if (button_toggled)
                  {
                      button_toggled = false;
                  }
                  else
                  {
                      button_toggled = true;
                  }

                  if( parent_toolbar != null)
                  {
                      parent_toolbar.setCurrentButton(b1)
                  }
                  back.color = button_toggled?color_pressed:color_normal
                  b1.clicked();
              //back.color = button_toggled?"white":"gray"
              }
              onReleased: {
                 // button_text.color='red'
              }
              onEntered:{
                  back.color = button_toggled?color_pressed:color_hover_normal
                  clicked_sign.color = button_toggled?"#FF9F00":color_hover_normal
                  //back.border.color =  button_toggled ? "#F88379" :"#FE6F5E"

              }
              onExited: {
                  back.color = button_toggled?color_pressed:color_normal
                  clicked_sign.color = button_toggled?"#FF9F00":color_normal

                  //back.border.color =  button_toggled ? "#F88379" :"transparent"

              }
              }
                }
          }
