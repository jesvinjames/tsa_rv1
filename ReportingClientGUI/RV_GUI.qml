import QtQuick 2.5
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4 as Controls14

import QtQuick.Layouts 1.3
import QtQml.Models 2.2

import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Private 1.0

import QtQuick.Controls 2.1
import QtQuick.Extras 1.4

import QtQuick 2.7

import QtWebEngine 1.3

import org.sdc.tsarv.template 1.0
import org.sdc.tsarv.chartmanager 1.0
import org.sdc.tsarv.strategymanager 1.0


ApplicationWindow {
    id:window
    visible: true
    title: "TSAPI"
    property int margin: 11
    property int seperator_size: 2
    property string seperator_color:"#343434"
    property string template_string:""
    property int current_window_index:0
    property string strategy_path:""
    property bool chart_loaded:false







   /* MouseArea {
        anchors.fill: parent

        onWheel: {

            console.debug("mouse scroll")
            wheel.accepted = true
            if (wheel.modifiers & Qt.ControlModifier) {
                console.debug("mouse scroll done")

                if( rv_toolbar.current_button == 2)
                {
                    if(zoom_factor+0.25 < 5)
                    {
                        zoom_factor += 0.25
                    }
                }


            }
        }
    }*/

    width: 1024
    height: 768

    ChartManager{
        id: chart_manager
    }

    StrategyManager{
        id:strategy_manager
    }


    function loadTemplates(strategy_path)
    {
        /*this.strategy_path = strategy_path
        html_template_info_strategy.set_template("res/templates/template_intrument_info.html")
        html_template_info_instrument.set_template("res/templates/template_intrument_info.html")
        html_template_info_strategy_instrument.set_template("res/templates/template_strat_intr_info.html")

        html_template_performance.set_template("res/templates/template_performance.html")
        html_template_risk.set_template("res/templates/template_risk.html")
        html_template_returns.set_template("res/templates/template_returns.html")




        strategy_manager.open_strategy(strategy_path)*/
        chart_loaded = false
        strategy_manager.open_strategy(strategy_path)
        //chart_manager.set_instrument_number(0)
        chart_manager.set_strategy_path(strategy_path)

        rv_toolbar.clear_instrument_combo()
        rv_toolbar.add_instrument_to_nav("Instrument All")
        rv_toolbar.set_strategy_name(strategy_manager.get_strategy_name())

        for(var i=1;i<=strategy_manager.get_instrument_count();i++){
            rv_toolbar.add_instrument_to_nav("Instrument " + i)

        }

        rv_toolbar.select_first_instrument()



       //content_charts.runJavaScript(chart_manager.get_price_chart_js_string(100,400))
       //TODO: navigationBar.navigation_bar.setStrategy(html_template_returns.get_strategy_name())



    }

    function loadMetrics(strategy_path){

    }

    function loadChart(action){

        switch(action){
        case 0:

            if(!chart_loaded)
            {
                content_charts.loadHtml(chart_manager.get_price_chart_html_string(),"qrc:/")

                content_charts.runJavaScript(chart_manager.get_price_chart_js_string(0),"qrc:/")
                chart_loaded = true
            }


            //content_charts.loadHtml(chart_manager.get_price_chart_html_string(),"qrc:/")
            //content_charts.runJavaScript(chart_manager.get_price_chart_js_string(0),"qrc:/")

           // content_template_performance.loadHtml(strategy_manager.get_template_string("performance"),"qrc:/");
           // console.debug(strategy_manager.get_template_string("performance"),"qrc:/")
           // content_template_performance.runJavaScript(chart_manager.get_price_chart_js_string(1),"qrc:/")

           //content_template_performance.runJavaScript(strategy_manager.get_equity_chart_string(),"qrc:/")
            break;
        case 1:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(1),"qrc:/")
            break;
        case 2:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(2),"qrc:/")
            break;
        case 3:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(3),"qrc:/")
            break;
        case 4:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(4),"qrc:/")
            break;
        case 5:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(5),"qrc:/")
            break;
        case 6:
            content_charts.runJavaScript(chart_manager.get_price_chart_js_string(6),"qrc:/")
            break;
        }
    }

    function loadInfoTemplate(){
        //html_template_info_strategy.load_metrics(strategy_manager.get_metrics(0))
        //content_template_info.loadHtml("<html>hi</html>");
        content_template_info.loadHtml(strategy_manager.get_template_string("info-strategy"),"qrc:/")
    }

    function loadPerformanceTemplate(){
       /* html_template_performance.load_metrics(this.strategy_path)
        content_template_performance.loadHtml(html_template_performance.get_template_string(),"qrc:/")*/

        //console.debug(content)
        content_template_performance.loadHtml(strategy_manager.get_template_string("performance"),"qrc:/");
       // console.debug(strategy_manager.get_template_string("performance"),"qrc:/")
       //content_template_performance.runJavaScript("","qrc:/")
       // content_charts.runJavaScript(chart_manager.get_price_chart_js_string(0),"qrc:/")


      // content_template_performance.runJavaScript(strategy_manager.get_histogram_chart_string(),"qrc:/")
       //content_template_performance.runJavaScript("")
     //  content_template_performance.runJavaScript(strategy_manager.get_equity_chart_string(),"qrc:/")



    }

    function loadRiskTemplate(){
        /*html_template_risk.load_metrics(this.strategy_path)
        content_template_risk.loadHtml(html_template_risk.get_template_string(),"qrc:/")*/
       // content_charts.runJavaScript(chart_manager.get_price_chart_js_string(0),"qrc:/")

        content_template_risk.loadHtml(strategy_manager.get_template_string("risk"),"qrc:/");
        //content_template_risk.runJavaScript(strategy_manager.get_capital_percentage_chart_string(),"qrc:/")

    }

    function loadReturnsTemplate(){
        /*html_template_returns.load_metrics(this.strategy_path)
        content_template_returns.loadHtml(html_template_returns.get_template_string(),"qrc:/")*/
        content_template_returns.loadHtml(strategy_manager.get_template_string("returns"),"qrc:/");
    }

    Component.onCompleted: {


        //console.log(content_web_engine.url)
        //loadTemplates()
        //content_charts.loadHtml("<html>Hi</html>")
        strategy_manager.init_templates()

       }

/*RVDrawer{
    id:rv_settings_drawer

}*/

Rectangle{

    property real zoom_factor: 1.0
    property real zoom_factor_rate: 0.1

    id:main_rect
    anchors.fill: parent
    border.color: seperator_color
    border.width: 4


    Keys.onPressed: {
        if( rv_toolbar.current_button == 1)
        {
             if ((event.key == Qt.Key_Plus) && (event.modifiers & Qt.ShiftModifier))
             {
                 console.debug("+++++++++++++")
                     if(zoom_factor+zoom_factor_rate <= 5)
                     {
                         zoom_factor += zoom_factor_rate
                     }
             }

             if ((event.key == Qt.Key_Minus) || (event.key == Qt.Key_Underscore ) && (event.modifiers & Qt.ShiftModifier))
             {
                 console.debug("------------------")

                 if(zoom_factor-zoom_factor_rate >= 0.25)
                 {
                     zoom_factor -= zoom_factor_rate
                 }

             }

             console.debug("zoom factor:" + zoom_factor)
        }
        }

ColumnLayout {
    spacing: 0
    id:main_layout
    anchors.fill: parent

    anchors.margins: 0

    /*Rectangle{
        color: seperator_color
 Layout.fillWidth: true
 Layout.minimumHeight: seperator_size
 Layout.maximumHeight: seperator_size
    }*/

   RVToolBar {

    id:rv_toolbar
    contents_stack:contents_stack
    bg_color:"#202024"
    Layout.fillWidth: true
    settings_drawer:rv_settings_drawer
    }



  /* Rectangle{
       color: "#DFDFDF"
       Layout.fillWidth: true
       Layout.maximumHeight: 5
       Layout.minimumHeight: 5
   }*/


    /*Rectangle{
        color: "#DFDFDF"
        Layout.fillWidth: true
        Layout.maximumHeight: 5
        Layout.minimumHeight: 5
    }*/





ColumnLayout {
    spacing: 0

    Layout.fillWidth: true






   RowLayout {
       anchors.margins: 0

       spacing: 0




                   id: rowLayout
                   anchors.fill: parent

                  /* Rectangle{
                       Layout.fillHeight: true
                       Layout.minimumWidth: seperator_size
                       Layout.maximumWidth: seperator_size
                       color: seperator_color
                   }*/



                   StackLayout{

                       id:menu_stack
                       Layout.maximumWidth: 150

                       currentIndex:rv_toolbar.current_button

                       SideMenu_File{
                           id:menu_file
                           Layout.maximumWidth: 150

                            Layout.margins: 0
                            //bg_color: seperator_color
                       }

                       SideMenu_Strategies{
                           id:menu_strategies
                           Layout.maximumWidth: 150
                            Layout.margins: 0
                            //bg_color: seperator_color
                       }

                       SideMenu_Charts{
                           id:menu_charts
                           Layout.maximumWidth: 150
                            Layout.margins: 0
                       }


                   }



                   /*Rectangle{
                       Layout.fillHeight: true
                       Layout.maximumWidth: 1
                       Layout.minimumWidth: 1
                       color: "#1883D7"

                   }*/



                   /*Rectangle{
                       Layout.fillHeight: true
                       Layout.minimumWidth: seperator_size
                       Layout.maximumWidth: seperator_size
                       color: seperator_color
                   }*/
                   Rectangle
                   {
                       id: content
                       Layout.fillHeight: true
                       Layout.fillWidth: true
                       Layout.rightMargin: 4
                       Layout.bottomMargin: 4

                        border.color: "transparent"
                        color:"#EFEFEF"

                        ColumnLayout {
                            spacing: 0
                            anchors.fill: parent
                            anchors.margins: 0
                        Rectangle{
                            id:control_bar
                            Layout.minimumHeight: 25
                            Layout.fillWidth: true
                            color:"lightgray"
                            visible: false
                        }

                        StackLayout{
                        id:contents_stack
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        currentIndex:rv_toolbar.current_button



                        FileOpenStrategies{

                            container: content
                            rc_gui:window
                            controller: rv_toolbar
                        }

                        StackLayout{
                        id:strategy_contents_stack
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        currentIndex: menu_strategies.current_button
                        WebEngineView
                        {
                            id:content_template_info
                             width: content.width
                             height: content.height
                             //url:"template_performance.html"
                             zoomFactor: main_rect.zoom_factor
                        }

                        WebEngineView
                        {

                            id:content_template_performance
                            width: content.width
                            height: content.height
                            // url:"google.com"
                            zoomFactor: main_rect.zoom_factor




                        }

                        WebEngineView
                        {
                            id:content_template_risk
                             width: content.width
                             height: content.height
                             zoomFactor: main_rect.zoom_factor

                        }

                        WebEngineView
                        {
                            id:content_template_returns
                             width: content.width
                             height: content.height
                             zoomFactor: main_rect.zoom_factor

                        }

                    }

                        ColumnLayout {
                            spacing: 0
                            id:chart_layout
                            anchors.fill: parent

                            anchors.margins: 0


                            RowLayout {
                                id: topmenu
                                Layout.fillWidth: true
                                implicitHeight: 30
                                Layout.minimumHeight: 30

                                spacing: 0

                                Button{

                                    Layout.fillHeight: true
                                    text:"First"

                                    onClicked: {
                                        window.loadChart(1)

                                    }
                                }
                                Button{
                                    Layout.fillHeight: true
                                    text:"Previous"

                                    onClicked: {
                                        window.loadChart(2)

                                    }
                                }
                                Button{
                                    Layout.fillHeight: true
                                    text:"Next"

                                    onClicked: {
                                        window.loadChart(3)

                                    }
                                }
                                Button{
                                    Layout.fillHeight: true
                                    text:"Last"

                                    onClicked: {
                                        window.loadChart(4)

                                    }
                                }
                                Button{
                                    Layout.fillHeight: true
                                    text:"Zoomin"

                                    onClicked: {
                                        window.loadChart(5)

                                    }
                                }
                                Button{
                                    Layout.fillHeight: true
                                    text:"Zoomout"

                                    onClicked: {
                                        window.loadChart(6)

                                    }
                                }


                            }

                            WebEngineView
                            {
                                 id:content_charts
                                 implicitWidth:  chart_layout.width
                                 implicitHeight: chart_layout.height-30

                                 Layout.maximumHeight: chart_layout.height-30

                            }
                        }





                    }
                        }
                   }

               }
  /* Controls14.TreeView {
       Controls14.TableViewColumn {
           title: "Name"
           role: "fileName"
           width: 300
       }
       Controls14.TableViewColumn {
           title: "Permissions"
           role: "filePermissions"
           width: 100
       }
       model: fileSystemModel
   }*/

 /*  ListView {
           anchors.fill: parent
           anchors.margins: 20

           clip: true

           model: spaceMen

           delegate: spaceManDelegate

           section.property: "nation"
           section.delegate: sectionDelegate
       }*/

       Component {
           id: spaceManDelegate

           Item {
               width: ListView.view.width
               height: 20

               RowLayout {
                   id: topmenu
                   Layout.fillWidth: true
                   spacing: 0


                    RVToggleButton{
                        text:name;
                    }

                    RVToggleButton{
                        text:nation;
                    }
           }}
       }



       Component {
           id: sectionDelegate

           Button {
               width: ListView.view.width
               height: 20
               text: section
           }
       }


       ListModel {
           id: spaceMen

           ListElement { name: "Abdul Ahad Mohmand"; nation: "Afganistan"; }
           ListElement { name: "Marcos Pontes"; nation: "Brazil"; }
           ListElement { name: "Alexandar Panayotov Alexandrov"; nation: "Bulgaria"; }
           ListElement { name: "Georgi Ivanov"; nation: "Bulgaria"; }
           ListElement { name: "Roberta Bondar"; nation: "Canada"; }
           ListElement { name: "Marc Garneau"; nation: "Canada"; }
           ListElement { name: "Chris Hadfield"; nation: "Canada"; }
           ListElement { name: "Guy Laliberte"; nation: "Canada"; }
           ListElement { name: "Steven MacLean"; nation: "Canada"; }
           ListElement { name: "Julie Payette"; nation: "Canada"; }
           ListElement { name: "Robert Thirsk"; nation: "Canada"; }
           ListElement { name: "Bjarni Tryggvason"; nation: "Canada"; }
           ListElement { name: "Dafydd Williams"; nation: "Canada"; }
       }




 /*   ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: margin

        GroupBox {
            id: gridBox
            title: "Grid layout"
            Layout.fillWidth: true

            GridLayout {
                id: gridLayout
                rows: 3
                flow: GridLayout.TopToBottom
                anchors.fill: parent

                Label { text: "Line 1" }
                Label { text: "Line 2" }
                Label { text: "Line 3" }

                TextField { }
                TextField { }
                TextField { }

                TextArea {
                    text: "This widget spans over three rows in the GridLayout.\n"
                        + "All items in the GridLayout are implicitly positioned from top to bottom."
                    Layout.rowSpan: 3
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }
            }
        }
        TextArea {
            id: t3
            text: "This fills the whole cell"
            Layout.minimumHeight: 30
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        GroupBox {
            id: stackBox
            title: "Stack layout"
            implicitWidth: 200
            implicitHeight: 60
            Layout.fillWidth: true
            Layout.fillHeight: true
            StackLayout {
                id: stackLayout
                anchors.fill: parent

                function advance() { currentIndex = (currentIndex + 1) % count }

                Repeater {
                    id: stackRepeater
                    model: 5
                    Rectangle {
                        color: Qt.hsla((0.5 + index)/stackRepeater.count, 0.3, 0.7, 1)
                        Button { anchors.centerIn: parent; text: "Page " + (index + 1); onClicked: { stackLayout.advance() } }
                    }
                }
            }
        }
    }*/
}
}
}
}
