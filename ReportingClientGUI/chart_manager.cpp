#include "chart_manager.h"

namespace tsarv {


chart_manager::chart_manager(QObject *parent) : QObject(parent)
{
   /* price_chart_html_string = R"V0G0N("

                              <html>
                              <head>


                              <script type="text/javascript" src ="amcharts.js"></script>
                              <script type="text/javascript" src = "serial.js"></script>
                              <script type="text/javascript" src="amstock.js"></script>
                              <script type="text/javascript" src ="/plugins/export/export.min.js"></script>

                              <link rel = "stylesheet" href ="plugins/export/export.css"type = "text / css" media = "all" / >
                              <script src = "themes/light.js"></script>

                              </head>
                              <div id='chartdiv'>Hi</div>

                              </html>

    )V0G0N";*/

    price_chart_html_string = "";
    price_chart_html_string += "<style>\n#chartdiv{\nwidth: 100 % ;height: 900px;}</style>\n"
        "<script src =\"amcharts.js\"></script>\n"
        "<script src = \"serial.js\"></script>\n"
        "<script src=\"amstock.js\"></script>\n"
        "<script src =\"plugins/export/export.min.js\"></script>\n";

    price_chart_html_string += "<link rel = \"stylesheet\" href =\"plugins/export/export.css\"type = \"text / css\" media = \"all\" / >"
        "<script src = \"themes/light.js\"></script><div id = \"chartdiv\"></div>";

    m_range_low = 0;
    m_range_high = 200;
    m_num_of_bars = 0 ;

    m_zoom_factor = 10;
    m_range_low = -20;
    m_range_high = 0;
    m_interval = 200;

    //std::cout<<"\n\n"<<price_chart_html_string<<"\n";
}

QString chart_manager::get_price_chart_html_string()
{
    return price_chart_html_string.c_str();
}

void chart_manager::set_strategy_path(QString strategy_path)
{
    try
    {

        //How to close database
        std::cout<<"\n\nopening db\n";
        m_strategy_path = strategy_path.toStdString() ;
        m_database = new tsa::fast::database(strategy_path.toStdString() +  "/log_db", tsa::no_create_db);
        //m_instr_table_name = "instrument_0_trade_log";
    }
    catch(std::exception e){
        std::cout<<"\nException : " <<e.what();
    }
}

void chart_manager::set_metrices(tsa::metrics* strategy_metrics,tsa::metrics* instrument_metrics )
{
    this->m_strategy_metrics = strategy_metrics;
    this->m_instrument_metrics = instrument_metrics;
}



void chart_manager::set_instrument_number(int instr_no)
{
    m_instr_no = instr_no;
    //m_instr_table_name = "instrument_" + std::to_string(instr_no) + "_trade_log";

}

QString chart_manager::get_price_chart_js_string(int action)
{
    tsa::record_set rset;
    std::string js_string = ";";


    switch(action)
    {
    case 0:
    {
        m_range_low = 0;
        m_range_high = 200;

        try
        {

            //this->m_database->series_load(m_instr_table_name, rset, m_range_low, m_range_high);//.mem_table("instrument_0_transaction_log");

            m_chart_manager.select_chart(m_strategy_path, 0, *m_database);
            m_chart_manager.set_range( m_range_low, m_range_high);
            m_chart_manager.prepare_chart_data_for_web_charts();
            m_num_of_bars = m_chart_manager.db_table_size();
            std::cout<<"\nNum Of Bars: "<<m_num_of_bars;

            m_zoom_factor = m_num_of_bars * 0.02;
            std::cout<<"\nZoom factor: "<<m_zoom_factor;

            m_interval = 200;
        }
        catch(std::exception e){
            std::cout<<"\nINIT EXCEPTION: " <<e.what()<<std::flush;
        }


    }break;
    case 1:
    {
        m_range_low = 0;
        m_range_high = m_interval;
    }break;
    case 2://prev
    {
        if (m_range_low >= m_interval)
        {
                    m_range_low -= m_interval;
                    m_range_high -= m_interval;
        }
        else if (m_range_low > 0)
        {
                    m_range_low = 0;
                    m_range_high = m_interval;
        }

    }break;
    case 3://next
    {
        if (m_range_high + m_interval <= m_num_of_bars)
        {
            m_range_low += m_interval;
            m_range_high += m_interval;
        }

    }break;
    case 4://last
    {
        m_range_low = m_num_of_bars - m_interval;
        m_range_high = m_num_of_bars;

    }break;
    case 5://zoomin
    {

        if (m_interval > 20 && m_range_high >= m_interval && (m_range_low + m_zoom_factor / 2) < (m_range_high - m_zoom_factor / 2))
        {
              m_interval -= m_zoom_factor;
              m_range_high -= m_zoom_factor / 2;
              m_range_low += m_zoom_factor / 2;
        }
    }break;
    case 6://zoomout
    {
        if (m_range_high < m_num_of_bars && m_interval <= 1000 && m_range_low >= m_zoom_factor / 2 && (m_range_low - m_zoom_factor / 2) >= 0)
        {
             m_interval += m_zoom_factor;
             m_range_high += m_zoom_factor / 2;
             m_range_low -= m_zoom_factor / 2;
        }
    }break;
    default:
    {
        m_range_low = 0;
        m_range_high = 200;
    }break;
    }

    try
    {
        //m_database->series_load(m_instr_table_name, rset, m_range_low, m_range_high);//.mem_table("instrument_0_transaction_log");

        m_chart_manager.select_chart(m_strategy_path, 0, *m_database);
        m_chart_manager.set_range( m_range_low, m_range_high);
        m_chart_manager.prepare_chart_data_for_web_charts();
        const tsa::record_set& data_ms = m_chart_manager.chart_data__millisec();
        json_chart m_json_chart;

        js_string = m_json_chart.get_price_chart_string(m_chart_manager,data_ms);
        //std::cout<<js_string;


    }
    catch(std::exception e){
        std::cout<<"\nEXCEPTION: " <<e.what()<<std::flush;
    }

    std::cout<<"\n\n\n\nPrice\n\n\n\n"<<js_string<<"\n\n\n"<<std::flush;

    return js_string.c_str();
}

QString chart_manager::get_dummy_price_chart()
{
    return "AmCharts.makeChart('equity_chart',{'categoryAxesSettings':{'autoGridCount':true,'dateFormats':[{'format':'JJ:NN:SS:QQQ','period':'fff'},{'format':'JJ:NN:SS:QQQ','period':'ss'},{'format':'JJ:NN:SS:QQQ','period':'mm'},{'format':'JJ:NN:SS:QQQ','period':'hh'},{'format':'YYYY-MM-DD','period':'DD'},{'format':'YYYY-MM-DD','period':'MM'},{'format':'YYYY-MM-DD','period':'YYYY'}],'equalSpacing':true,'groupToPeriods':['fff'],'minPeriod':'fff','startOnAxis':false},'chartCursorSettings':{'categoryBalloonDateFormats':[{'format':'YYYY-MM-DD JJ:NN:SS:QQQ','period':'fff'}],'categoryBalloonEnabled':true,'categoryBalloonText':'[[category]]','cursorColor':'rgb(200,0,0)','valueBalloonsEnabled':true,'valueLineAlpha':0.25,'valueLineBalloonEnabled':true,'valueLineEnabled':true,'zoomable':false},'chartScrollbarSettings':{'enabled':false},'dataDateFormat':'YYYY-MM-DDTJJ:NN:SS:QQQ','dataSets':[{'categoryField':'price_Date','dataProvider':[{'0':'1009.0','1':'1019.79999999999995453','10':'255255255','2':'1003.5','3':'1019.0','4':'1020.53614997713782486','5':'1031.24209616829443803','6':'1009.0','7':'1.0','8':'0','9':'0.0','price_Date':'2002-06-12T13:27:00:000'}],'fieldMappings':[{'fromField':'0','toField':'0'},{'fromField':'1','toField':'1'},{'fromField':'2','toField':'2'},{'fromField':'3','toField':'3'},{'fromField':'4','toField':'4'},{'fromField':'5','toField':'5'},{'fromField':'6','toField':'6'},{'fromField':'7','toField':'7'},{'fromField':'8','toField':'8'},{'fromField':'9','toField':'9'},{'fromField':'10','toField':'10'}]}],'panels':[{'percentHeight':300.0,'stockGraphs':[{'balloonText':'Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>','closeField':'3','fillAlphas':0,'fillColors':'rgb(255,215,0)','highField':'1','lineAlpha':1,'lineColor':'rgb(255,215,0)','lineThickness':3,'lowField':'2','negativeFillColors':'rgb(255,215,0)','negativeLineColor':'rgb(255,215,0)','openField':'0','type':'ohlc','useDataSetColors':false,'valueField':'3'},{'fillAlphas':0,'lineColor':'','lineThickness':2,'showBalloon':false,'title':'EMA(5)','type':'line','useDataSetColors':false,'valueField':'4'},{'fillAlphas':0,'lineColor':'','lineThickness':2,'showBalloon':false,'title':'EMA(10)','type':'line','useDataSetColors':false,'valueField':'5'}],'stockLegend':{},'title':'','trendLines':[{'finalDate':'2002-06-13T13:03:00.0','finalValue':0.0,'initialDate':'2002-06-13T13:03:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000001<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'balloonText':'order_id:<b>1000000002<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1024.3<br>quantity:3','finalDate':'2002-06-13T13:22:00.0','finalValue':1024.3,'initialDate':'2002-06-13T13:03:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000002<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1024.3<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1024.3,'lineColor':'#0000FF','lineThickness':1},{'balloonText':'order_id:<b>1000000003<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1018.3<br>quantity:3','finalDate':'2002-06-13T13:22:00.0','finalValue':1018.3000000000001,'initialDate':'2002-06-13T13:03:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000003<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1018.3<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1018.3000000000001,'lineColor':'#c00','lineThickness':1},{'finalDate':'2002-06-13T14:19:00.0','finalValue':0.0,'initialDate':'2002-06-13T14:19:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000004<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-14T12:14:00.0','finalValue':0.0,'initialDate':'2002-06-14T12:14:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000005<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'balloonText':'order_id:<b>1000000006<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1004.3<br>quantity:3','finalDate':'2002-06-14T12:25:00.0','finalValue':1004.3000000000001,'initialDate':'2002-06-14T12:14:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000006<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1004.3<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1004.3000000000001,'lineColor':'#0000FF','lineThickness':1},{'balloonText':'order_id:<b>1000000007<br>instrument_id:1<br>action:sell<br>type:stop<br>price:998.3<br>quantity:3','finalDate':'2002-06-14T12:25:00.0','finalValue':998.3000000000001,'initialDate':'2002-06-14T12:14:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000007<br>instrument_id:1<br>action:sell<br>type:stop<br>price:998.3<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':998.3000000000001,'lineColor':'#c00','lineThickness':1},{'finalDate':'2002-06-14T13:01:00.0','finalValue':0.0,'initialDate':'2002-06-14T13:01:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000008<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-14T15:11:00.0','finalValue':0.0,'initialDate':'2002-06-14T15:11:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000009<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-14T15:11:00.0','finalValue':1010.2,'initialDate':'2002-06-14T15:11:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000010<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1010.2<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1010.2,'lineColor':'#0000FF','lineThickness':0},{'finalDate':'2002-06-14T15:11:00.0','finalValue':1004.2,'initialDate':'2002-06-14T15:11:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000011<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1004.2<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1004.2,'lineColor':'#c00','lineThickness':0},{'finalDate':'2002-06-18T08:52:00.0','finalValue':0.0,'initialDate':'2002-06-18T08:52:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000012<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-18T11:20:00.0','finalValue':0.0,'initialDate':'2002-06-18T11:20:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000013<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'balloonText':'order_id:<b>1000000014<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1042.5<br>quantity:3','finalDate':'2002-06-18T11:29:00.0','finalValue':1042.5,'initialDate':'2002-06-18T11:20:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000014<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1042.5<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1042.5,'lineColor':'#0000FF','lineThickness':1},{'balloonText':'order_id:<b>1000000015<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1036.5<br>quantity:3','finalDate':'2002-06-18T11:29:00.0','finalValue':1036.5,'initialDate':'2002-06-18T11:20:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000015<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1036.5<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1036.5,'lineColor':'#c00','lineThickness':1},{'finalDate':'2002-06-18T13:24:00.0','finalValue':0.0,'initialDate':'2002-06-18T13:24:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000016<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-18T21:51:00.0','finalValue':0.0,'initialDate':'2002-06-18T21:51:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000017<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'balloonText':'order_id:<b>1000000018<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1044<br>quantity:3','finalDate':'2002-06-19T09:34:00.0','finalValue':1044.0,'initialDate':'2002-06-18T21:51:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000018<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1044<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1044.0,'lineColor':'#0000FF','lineThickness':1},{'balloonText':'order_id:<b>1000000019<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1038<br>quantity:3','finalDate':'2002-06-19T09:34:00.0','finalValue':1038.0,'initialDate':'2002-06-18T21:51:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000019<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1038<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1038.0,'lineColor':'#c00','lineThickness':1},{'finalDate':'2002-06-19T09:41:00.0','finalValue':0.0,'initialDate':'2002-06-19T09:41:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000020<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-19T12:26:00.0','finalValue':0.0,'initialDate':'2002-06-19T12:26:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000021<br>instrument_id:1<br>action:buy<br>type:market<br>price:0<br>quantity:10','color':'#0c0','height':10,'offsetX':5,'offsetY':6,'rotation':270,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0},{'finalDate':'2002-06-19T12:26:00.0','finalValue':1040.2,'initialDate':'2002-06-19T12:26:00.0','initialImage':{'balloonColor':'#0000FF','balloonText':'order_id:<b>1000000022<br>instrument_id:1<br>action:sell<br>type:limit<br>price:1040.2<br>quantity:3','color':'#0000FF','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1040.2,'lineColor':'#0000FF','lineThickness':0},{'finalDate':'2002-06-19T12:26:00.0','finalValue':1034.2,'initialDate':'2002-06-19T12:26:00.0','initialImage':{'balloonColor':'#c00','balloonText':'order_id:<b>1000000023<br>instrument_id:1<br>action:sell<br>type:stop<br>price:1034.2<br>quantity:3','color':'#c00','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':1034.2,'lineColor':'#c00','lineThickness':0},{'finalDate':'2002-06-19T13:02:00.0','finalValue':0.0,'initialDate':'2002-06-19T13:02:00.0','initialImage':{'balloonColor':'#0c0','balloonText':'order_id:<b>1000000024<br>instrument_id:1<br>action:sell<br>type:market<br>price:0<br>quantity:7','color':'#0c0','height':10,'offsetX':5,'offsetY':-5,'rotation':90,'svgPath':'M0,0 L0,2 L2,1 Z','width':10},'initialValue':0.0,'lineColor':'#0c0','lineThickness':0}]},{'percentHeight':150.0,'stockGraphs':[{'connect':true,'fillAlphas':1,'fillColors':'rgb(255,255,255)','showBalloon':false,'title':'Position','type':'column','useDataSetColors':false,'valueField':'9'}],'stockLegend':{},'title':'','trendLines':[]}],'panelsSettings':{'precision':6},'theme':'light','type':'stock','valueAxesSettings':{'autoGridCount':true,'position':'right'}});";
}

chart_manager::~chart_manager()
{
    try
    {
       delete  m_database;
    }
    catch(std::exception e){
        std::cout<<"\nException : " <<e.what();
    }
}
}
