#ifndef TEST_PLUGGIN_H
#define TEST_PLUGGIN_H

#include <QQmlExtensionPlugin>

class test_pluggin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.sdc.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif // TEST_PLUGGIN_H
