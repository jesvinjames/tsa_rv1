import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import org.sdc.people 1.0

import QtWebView 1.1
import QtWebEngine 1.0
import QtQuick.Controls.Styles 1.4


ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Person {
        name: "Bob Jones"
        shoeSize: 12
        id:p1
    }



    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            RowLayout {
                id: layout
                anchors.fill: parent
                spacing: 1

                Rectangle {
                    anchors.fill: parent

                        color: 'teal'

                        Text {
                            text: parent.width + 'x' + parent.height
                        }
                    }

            TabView {
               id: tabwidget
               anchors.fill: parent
               tabPosition: Qt.LeftEdge


               style: TabViewStyle {
                      frameOverlap: 1
                      tab: Rectangle {
                          color: styleData.selected ? "steelblue" :"lightsteelblue"
                          border.color:  "steelblue"
                          implicitWidth: Math.max(text.width + 4, 80)
                          implicitHeight: 50
                          radius: 2
                          Text {
                              id: text
                              anchors.centerIn: parent
                              text: styleData.title
                              color: styleData.selected ? "white" : "black"
                          }
                      }
                      frame: Rectangle { color: "steelblue" }
                  }
               Tab {
                      title: "Charts"
                      Rectangle { color: "green"



                          WebEngineView {
                                  anchors.fill: parent
                                  url: "file:///release/test.html"

                              }

                      }

                  }
                  Tab {
                      title: "Summary"
                      Rectangle { color: "blue"

                          WebEngineView {
                                  anchors.fill: parent
                                  url: "file:///release/res/amstockcharts/histogram5.html"

                              }
                      }
                  }
                  Tab {
                      title: "Trades"
                      Rectangle { color: "green" }
                  }
                  Tab {
                      title: "Risk"
                      Rectangle { color: "green" }
                  }
                  Tab {
                      title: "Units"
                      Rectangle { color: "green" }
                  }

               }
            }

        }

        Page {
            Label {
                text: qsTr("Second page")
                anchors.centerIn: parent
            }
        }
        Page {


            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                spacing: 5

            Button{
                id:button1
                Layout.alignment: Qt.AlignCenter
                text:"Open"
            }
            Button{
                id:button2
                Layout.alignment: Qt.AlignCenter
                text:"Close"
            }
            }
        }
    }

    header: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Strategy")
        }
        TabButton {
            text: qsTr("Instruments")
        }
        TabButton {
            text: qsTr("File")

        }
    }
}
