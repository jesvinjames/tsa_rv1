#pragma once
#include<iostream>
#include<sstream>
#include<fstream>

#include "tsc.h"

//#include "type_def.h"

class json_chart
{
public:
	struct properties {
		std::string chartdiv = "chartdiv";
		std::string type = "stock";
		std::string theme = "light";
		std::string	dataDateFormat = "YYYY-MM-DDTJJ:NN:SS:QQQ";
		struct
		{
			bool autoGridCount = true;
			bool equalSpacing = true;
			bool startOnAxis = false;
			std::string minPeriod = "fff";
		} categoryAxesSettings;
		struct
		{
			std::string position = "right";
			bool autoGridCount = true;
		}valueAxesSettings;
		struct
		{
			bool enabled = false;
		}chartScrollbarSettings;
		struct {
			double valueLineAlpha = 0.25;
			bool valueLineEnabled = true;
			bool zoomable = false;
			bool valueLineBalloonEnabled=true;
			bool valueBalloonsEnabled = true;
			bool categoryBalloonEnabled = true;
			std::string categoryBalloonText = "[[category]]";
			std::string cursorColor = "rgb(200,0,0)";
		}chartCursorSettings;
	};
private:
	tsa::json m_json = tsa::json::object{};
	properties m_properties;
public:
	json_chart(void);
	~json_chart(void);

	json_chart::properties& properties(void) { return m_properties; }
private:

	void setChartTheme();
	void setChartType();
	void setDataDateFormat();
	void setValueAxesSettings();
	void setChartScrollbarSettings();
	void setChart_Panels(const tsc::report_chart_manager&);
	void setChart_DataSets(const tsa::record_set&);
	void setCategoryAxesSettings();
	void setChartCursorSettings();
	void setPanelsSettings();

	void panel_set(const tsa::pane_and_plot_meta_data&, size_t panel_pos);
	void plot_ohlc(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_line(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_bar(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_Area(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_BulletLine(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_StepLine(const tsa::plot_properties&, size_t panel_pos, size_t plot_pos);
	void plot_GuideLines(const tsa::horiz_plot_guide_line&,size_t panel_pos,size_t guid_pos);

	void insert_plot_name_if_exists(const tsa::plot_properties&, tsa::json&);
	void plot_orders(const tsc::report_chart_manager::order_display_info&, size_t panel_pos,size_t order_pos);
public:
	void setChartDiv(const std::string&);
    std::string get_price_chart_string(const tsc::report_chart_manager&,const tsa::record_set&);
	std::string to_json_string();
};

