#ifndef HTML_TEMPLATE_H
#define HTML_TEMPLATE_H

#include <string>
#include <map>
#include <vector>
#include <regex>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <QDebug>


#include "html_template.h"
#include "sys_metrics.h"
#include "util_string.h"

namespace tsarv
{
    struct template_key
    {
       std::string key;
       std::string value;
       int position;
       int length;
    };

    class html_template
    {
    public:
        html_template();
        html_template(std::string template_file);

        ~html_template();

        bool load_keys();
        void reset_keys();
        void set_keys(std::vector<std::string> keys);
        void set_values(std::vector<std::string> values);
        bool set_template_string(std::string template_body);
        bool set_template(std::string template_file);
        std::string template_string();
        void clear();

        void processTemplate(tsa::metrics* cptrMetrics_i, tsa::metrics* cptrStrategyMetrics_i);

        std::string process();

        void replace_all( std::string &s, const std::string &search, const std::string &replace );

        bool is_loaded();
        void unload();

        void set_key(std::string key, std::string data);


    protected:
        std::vector<std::string> keys;
        std::vector<std::string> values;
        std::map<std::string,std::string> m_strsKeys;

        std::vector<template_key> key_chars;

        bool keys_loaded;
        int num_of_keys;
        const std::regex regex_expression;

        std::string initial_template_body;
        std::string template_body;

        bool m_loaded;


    private:
    };
}

#endif // HTML_TEMPLATE_H
