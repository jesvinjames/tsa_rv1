/****************************************************************************
** Meta object code from reading C++ file 'chart_manager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ReportingClientGUI/chart_manager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chart_manager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_tsarv__chart_manager_t {
    QByteArrayData data[10];
    char stringdata0[168];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_tsarv__chart_manager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_tsarv__chart_manager_t qt_meta_stringdata_tsarv__chart_manager = {
    {
QT_MOC_LITERAL(0, 0, 20), // "tsarv::chart_manager"
QT_MOC_LITERAL(1, 21, 17), // "set_strategy_path"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 13), // "strategy_path"
QT_MOC_LITERAL(4, 54, 21), // "set_instrument_number"
QT_MOC_LITERAL(5, 76, 8), // "instr_no"
QT_MOC_LITERAL(6, 85, 25), // "get_price_chart_js_string"
QT_MOC_LITERAL(7, 111, 6), // "action"
QT_MOC_LITERAL(8, 118, 27), // "get_price_chart_html_string"
QT_MOC_LITERAL(9, 146, 21) // "get_dummy_price_chart"

    },
    "tsarv::chart_manager\0set_strategy_path\0"
    "\0strategy_path\0set_instrument_number\0"
    "instr_no\0get_price_chart_js_string\0"
    "action\0get_price_chart_html_string\0"
    "get_dummy_price_chart"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_tsarv__chart_manager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x02 /* Public */,
       4,    1,   42,    2, 0x02 /* Public */,
       6,    1,   45,    2, 0x02 /* Public */,
       8,    0,   48,    2, 0x02 /* Public */,
       9,    0,   49,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::QString, QMetaType::Int,    7,
    QMetaType::QString,
    QMetaType::QString,

       0        // eod
};

void tsarv::chart_manager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        chart_manager *_t = static_cast<chart_manager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_strategy_path((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->set_instrument_number((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: { QString _r = _t->get_price_chart_js_string((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 3: { QString _r = _t->get_price_chart_html_string();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 4: { QString _r = _t->get_dummy_price_chart();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject tsarv::chart_manager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_tsarv__chart_manager.data,
      qt_meta_data_tsarv__chart_manager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *tsarv::chart_manager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *tsarv::chart_manager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_tsarv__chart_manager.stringdata0))
        return static_cast<void*>(const_cast< chart_manager*>(this));
    return QObject::qt_metacast(_clname);
}

int tsarv::chart_manager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
