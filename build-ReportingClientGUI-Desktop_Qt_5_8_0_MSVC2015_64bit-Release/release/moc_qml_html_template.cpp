/****************************************************************************
** Meta object code from reading C++ file 'qml_html_template.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ReportingClientGUI/qml_html_template.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qml_html_template.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_tsarv__qml__html_template_t {
    QByteArrayData data[10];
    char stringdata0[141];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_tsarv__qml__html_template_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_tsarv__qml__html_template_t qt_meta_stringdata_tsarv__qml__html_template = {
    {
QT_MOC_LITERAL(0, 0, 25), // "tsarv::qml::html_template"
QT_MOC_LITERAL(1, 26, 12), // "load_metrics"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 13), // "strategy_path"
QT_MOC_LITERAL(4, 54, 13), // "tsa::metrics*"
QT_MOC_LITERAL(5, 68, 7), // "metrics"
QT_MOC_LITERAL(6, 76, 19), // "get_template_string"
QT_MOC_LITERAL(7, 96, 12), // "set_template"
QT_MOC_LITERAL(8, 109, 13), // "template_file"
QT_MOC_LITERAL(9, 123, 17) // "get_strategy_name"

    },
    "tsarv::qml::html_template\0load_metrics\0"
    "\0strategy_path\0tsa::metrics*\0metrics\0"
    "get_template_string\0set_template\0"
    "template_file\0get_strategy_name"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_tsarv__qml__html_template[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x02 /* Public */,
       1,    1,   42,    2, 0x02 /* Public */,
       6,    0,   45,    2, 0x02 /* Public */,
       7,    1,   46,    2, 0x02 /* Public */,
       9,    0,   49,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::QString,
    QMetaType::Bool, QMetaType::QString,    8,
    QMetaType::QString,

       0        // eod
};

void tsarv::qml::html_template::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        html_template *_t = static_cast<html_template *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->load_metrics((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->load_metrics((*reinterpret_cast< tsa::metrics*(*)>(_a[1]))); break;
        case 2: { QString _r = _t->get_template_string();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->set_template((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { QString _r = _t->get_strategy_name();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject tsarv::qml::html_template::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_tsarv__qml__html_template.data,
      qt_meta_data_tsarv__qml__html_template,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *tsarv::qml::html_template::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *tsarv::qml::html_template::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_tsarv__qml__html_template.stringdata0))
        return static_cast<void*>(const_cast< html_template*>(this));
    if (!strcmp(_clname, "tsarv::html_template"))
        return static_cast< tsarv::html_template*>(const_cast< html_template*>(this));
    return QObject::qt_metacast(_clname);
}

int tsarv::qml::html_template::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
